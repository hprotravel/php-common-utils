## 0.16.1 (February 07, 2025)
  - Hotfix - CK-844 - Implicitly marking parameter as nullable is deprecated

## 0.16.0 (January 03, 2025)
  - Feature - CK-856 - Refactor CurrencyUtils
  - Feature - CK-856 - Add ext-intl and use NumberFormatter to format currencies
  - Feature - CK-856 - Improve adding exists helper
  - Feature - CK-856 - Adds some missing currency symbols
  - Feature - CK-856 - Add CurrencyUtils

## 0.15.0 (December 19, 2024)
  - Feature - CK-847 - Add \Compass\Utils\DateTimeUtils::intervalOfDates method

## 0.14.0 (November 14, 2024)
  - Feature - CK-764 - Remove blank char from default separator
  - Feature - CK-764 - Add \Compass\Utils\ObjectUtils::classHeadline method
  - Feature - CK-764 - Add \Compass\Utils\StringUtils::headline method to helpers.php
  - Feature - CK-764 - Add \Compass\Utils\Collection::join method

## 0.13.0 (October 16, 2024)
  - Feature - CK-721 - Add \Compass\Utils\DateTimeUtils::subtractInterval method

## 0.12.1 (September 05, 2024)
  - Fix attempt to call from global namespace "isset"

## 0.12.0 (August 22, 2024)
  - Feature - CK-634 - Add generateUrl and addQueryParams utils

## 0.11.0 (June 10, 2024)
  - Feature - CK-613 - Add escape string feature

## 0.10.0 (March 26, 2024)
  - Feature - CK-560 - Add Arrayable interface
  - Feature - CK-548 - Add string split and array join helper methods
  - Feature - CK-530 - Add LogUtils utility class

## 0.9.1 (February 09, 2024)
  - Hotfix - CK-498 - Fix unreadable.xml file permission

## 0.9.0 (February 09, 2024)
  - Feature - CK-498 - Add additional file helpers with using GitHub Copilot
  - Feature - CK-498 - Clean up ignored files
  - Feature - CK-498 - Add composer.lock to gitignore file
  - Feature - CK-498 - Add file json helper
  - Feature - CK-498 - Add file read helper
  - Feature - CK-498 - Add file exists helper
  - Feature - CK-498 - Add FileUtils helper class to utilize file processing

## 0.8.1 (January 22, 2024)
  - Bugfix - CK-473 - Add missing helpers

## 0.8.0 (January 22, 2024)
  - Bugfix - CK-473 - Add missing helpers

## 0.7.1 (January 15, 2024)
  - Hotfix - CK-473 - Remove the unsupported slug test case

## 0.7.0 (January 15, 2024)
  - Feature - CK-473 - Remove unnecessary Optional::present helper
  - Feature - CK-473 - Add NumberUtils::between helper
  - Feature - CK-473 - Add CommonUtils::retry helper
  - Feature - CK-473 - Improve UrlUtilsTest::testGetQueryParams test
  - Feature - CK-473 - Add extra tests for UrlUtils::addQueryParams
  - Feature - CK-473 - Add StringUtils::empty helper
  - Feature - CK-473 - Upgrade to PHP 8.2

## 0.6.0 (February 24, 2023)
  - Feature - CK-350 - Make final CommonUtils class
  - Feature - CK-350 - Refactor CommonUtilsTest class add data providers
  - Feature - CK-350 - Throw exception when utility class instantiate
  - Feature - CK-350 - Add UnsupportedOperationException and trait for constructor
  - Feature - CK-350 - Refactor Inflector tests move assertions to data providers
  - Feature - CK-350 - Rename starts and ends methods with starts_with and ends_with and add missing tests
  - Feature - CK-350 - Rename Hlp to CommonUtils and Move stripQuotes helper method to StringUtils
  - Feature - CK-350 - Rename Str to StringUtils
  - Feature - CK-350 - Rename Arr to ArrayUtils

## 0.5.0 (January 23, 2023)
  - Feature - CK-352 - Rename package

## 0.4.0 (November 21, 2022)
  - Feature - CK-305 - Add limit and remove line feed helper methods
  - Fix phpunit color and coverage options

## 0.3.0 (November 10, 2021)
  - Feature - CK-35 - Add Inflector class
  - Add .idea/ folder to .gitignore file

## 0.2.2 (October 26, 2021)
  - Remove -compass suffix from package name

## 0.2.1 (October 26, 2021)
  - Change package name with hprotravel-compass/php-utils
  - README.md edited online with Bitbucket

## 0.2.0 (October 26, 2021)
  - Change namespace with compass
  - Add hprotravel suffix to package name
  - Change package name to hprotravel/utils

## 0.1.0 (January 08, 2021)
  - Update composer
  - Change github actions workflow runs-on to ubuntu-18.04
  - Create composer phpunit test script
  - Create php.yml
  - Apply php-cs-fixer with symfony rule
  - Add utility classes
  - Add README file
  - Initial commit

