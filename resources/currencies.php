<?php

return [
    'AFN' => [
        'code' => 'AFN',
        'name' => 'Afghan Afghani',
        'symbol' => 'AFN',
    ],
    'ALL' => [
        'code' => 'ALL',
        'name' => 'Albanian Lek',
        'symbol' => 'ALL',
    ],
    'DZD' => [
        'code' => 'DZD',
        'name' => 'Algerian Dinar',
        'symbol' => 'DZD',
    ],
    'ADP' => [
        'code' => 'ADP',
        'name' => 'Andorran Peseta',
        'symbol' => 'ADP',
    ],
    'AOA' => [
        'code' => 'AOA',
        'name' => 'Angolan Kwanza',
        'symbol' => 'AOA',
    ],
    'ARA' => [
        'code' => 'ARA',
        'name' => 'Argentine Austral',
        'symbol' => 'ARA',
    ],
    'ARS' => [
        'code' => 'ARS',
        'name' => 'Argentine Peso',
        'symbol' => 'ARS',
    ],
    'AMD' => [
        'code' => 'AMD',
        'name' => 'Armenian Dram',
        'symbol' => 'AMD',
    ],
    'AWG' => [
        'code' => 'AWG',
        'name' => 'Aruban Florin',
        'symbol' => 'AWG',
    ],
    'AUD' => [
        'code' => 'AUD',
        'name' => 'Australian Dollar',
        'symbol' => 'A$',
    ],
    'ATS' => [
        'code' => 'ATS',
        'name' => 'Austrian Schilling',
        'symbol' => 'ATS',
    ],
    'AZN' => [
        'code' => 'AZN',
        'name' => 'Azerbaijani Manat',
        'symbol' => 'AZN',
    ],
    'BSD' => [
        'code' => 'BSD',
        'name' => 'Bahamian Dollar',
        'symbol' => 'BSD',
    ],
    'BHD' => [
        'code' => 'BHD',
        'name' => 'Bahraini Dinar',
        'symbol' => 'BHD',
    ],
    'BDT' => [
        'code' => 'BDT',
        'name' => 'Bangladeshi Taka',
        'symbol' => 'BDT',
    ],
    'BBD' => [
        'code' => 'BBD',
        'name' => 'Barbadian Dollar',
        'symbol' => 'BBD',
    ],
    'BYN' => [
        'code' => 'BYN',
        'name' => 'Belarusian Ruble',
        'symbol' => 'BYN',
    ],
    'BEF' => [
        'code' => 'BEF',
        'name' => 'Belgian Franc',
        'symbol' => 'BEF',
    ],
    'BEC' => [
        'code' => 'BEC',
        'name' => 'Belgian Franc (convertible)',
        'symbol' => 'BEC',
    ],
    'BEL' => [
        'code' => 'BEL',
        'name' => 'Belgian Franc (financial)',
        'symbol' => 'BEL',
    ],
    'BZD' => [
        'code' => 'BZD',
        'name' => 'Belize Dollar',
        'symbol' => 'BZD',
    ],
    'BMD' => [
        'code' => 'BMD',
        'name' => 'Bermudan Dollar',
        'symbol' => 'BMD',
    ],
    'BTN' => [
        'code' => 'BTN',
        'name' => 'Bhutanese Ngultrum',
        'symbol' => 'BTN',
    ],
    'VED' => [
        'code' => 'VED',
        'name' => 'Bolívar Soberano',
        'symbol' => 'VED',
    ],
    'BOB' => [
        'code' => 'BOB',
        'name' => 'Bolivian Boliviano',
        'symbol' => 'BOB',
    ],
    'BOV' => [
        'code' => 'BOV',
        'name' => 'Bolivian Mvdol',
        'symbol' => 'BOV',
    ],
    'BOP' => [
        'code' => 'BOP',
        'name' => 'Bolivian Peso',
        'symbol' => 'BOP',
    ],
    'BAM' => [
        'code' => 'BAM',
        'name' => 'Bosnia-Herzegovina Convertible Mark',
        'symbol' => 'BAM',
    ],
    'BWP' => [
        'code' => 'BWP',
        'name' => 'Botswanan Pula',
        'symbol' => 'BWP',
    ],
    'BRL' => [
        'code' => 'BRL',
        'name' => 'Brazilian Real',
        'symbol' => 'R$',
    ],
    'GBP' => [
        'code' => 'GBP',
        'name' => 'British Pound',
        'symbol' => '£',
    ],
    'BND' => [
        'code' => 'BND',
        'name' => 'Brunei Dollar',
        'symbol' => 'BND',
    ],
    'BGL' => [
        'code' => 'BGL',
        'name' => 'Bulgarian Hard Lev',
        'symbol' => 'BGL',
    ],
    'BGN' => [
        'code' => 'BGN',
        'name' => 'Bulgarian Lev',
        'symbol' => 'BGN',
    ],
    'BGM' => [
        'code' => 'BGM',
        'name' => 'Bulgarian Socialist Lev',
        'symbol' => 'BGM',
    ],
    'BUK' => [
        'code' => 'BUK',
        'name' => 'Burmese Kyat',
        'symbol' => 'BUK',
    ],
    'BIF' => [
        'code' => 'BIF',
        'name' => 'Burundian Franc',
        'symbol' => 'BIF',
    ],
    'KHR' => [
        'code' => 'KHR',
        'name' => 'Cambodian Riel',
        'symbol' => 'KHR',
    ],
    'CAD' => [
        'code' => 'CAD',
        'name' => 'Canadian Dollar',
        'symbol' => 'CA$',
    ],
    'CVE' => [
        'code' => 'CVE',
        'name' => 'Cape Verdean Escudo',
        'symbol' => 'CVE',
    ],
    'KYD' => [
        'code' => 'KYD',
        'name' => 'Cayman Islands Dollar',
        'symbol' => 'KYD',
    ],
    'XAF' => [
        'code' => 'XAF',
        'name' => 'Central African CFA Franc',
        'symbol' => 'FCFA',
    ],
    'XPF' => [
        'code' => 'XPF',
        'name' => 'CFP Franc',
        'symbol' => 'CFPF',
    ],
    'CLE' => [
        'code' => 'CLE',
        'name' => 'Chilean Escudo',
        'symbol' => 'CLE',
    ],
    'CLP' => [
        'code' => 'CLP',
        'name' => 'Chilean Peso',
        'symbol' => 'CLP',
    ],
    'CLF' => [
        'code' => 'CLF',
        'name' => 'Chilean Unit of Account (UF)',
        'symbol' => 'CLF',
    ],
    'CNX' => [
        'code' => 'CNX',
        'name' => 'Chinese People’s Bank Dollar',
        'symbol' => 'CNX',
    ],
    'CNY' => [
        'code' => 'CNY',
        'name' => 'Chinese Yuan',
        'symbol' => 'CN¥',
    ],
    'CNH' => [
        'code' => 'CNH',
        'name' => 'Chinese Yuan (offshore)',
        'symbol' => 'CNH',
    ],
    'COP' => [
        'code' => 'COP',
        'name' => 'Colombian Peso',
        'symbol' => 'COP',
    ],
    'COU' => [
        'code' => 'COU',
        'name' => 'Colombian Real Value Unit',
        'symbol' => 'COU',
    ],
    'KMF' => [
        'code' => 'KMF',
        'name' => 'Comorian Franc',
        'symbol' => 'KMF',
    ],
    'CDF' => [
        'code' => 'CDF',
        'name' => 'Congolese Franc',
        'symbol' => 'CDF',
    ],
    'CRC' => [
        'code' => 'CRC',
        'name' => 'Costa Rican Colón',
        'symbol' => 'CRC',
    ],
    'HRD' => [
        'code' => 'HRD',
        'name' => 'Croatian Dinar',
        'symbol' => 'HRD',
    ],
    'HRK' => [
        'code' => 'HRK',
        'name' => 'Croatian Kuna',
        'symbol' => 'HRK',
    ],
    'CUC' => [
        'code' => 'CUC',
        'name' => 'Cuban Convertible Peso',
        'symbol' => 'CUC',
    ],
    'CUP' => [
        'code' => 'CUP',
        'name' => 'Cuban Peso',
        'symbol' => 'CUP',
    ],
    'CYP' => [
        'code' => 'CYP',
        'name' => 'Cypriot Pound',
        'symbol' => 'CYP',
    ],
    'CZK' => [
        'code' => 'CZK',
        'name' => 'Czech Koruna',
        'symbol' => 'CZK',
    ],
    'CSK' => [
        'code' => 'CSK',
        'name' => 'Czechoslovak Hard Koruna',
        'symbol' => 'CSK',
    ],
    'DKK' => [
        'code' => 'DKK',
        'name' => 'Danish Krone',
        'symbol' => 'DKK',
    ],
    'DJF' => [
        'code' => 'DJF',
        'name' => 'Djiboutian Franc',
        'symbol' => 'DJF',
    ],
    'DOP' => [
        'code' => 'DOP',
        'name' => 'Dominican Peso',
        'symbol' => 'DOP',
    ],
    'NLG' => [
        'code' => 'NLG',
        'name' => 'Dutch Guilder',
        'symbol' => 'NLG',
    ],
    'XCD' => [
        'code' => 'XCD',
        'name' => 'East Caribbean Dollar',
        'symbol' => 'EC$',
    ],
    'DDM' => [
        'code' => 'DDM',
        'name' => 'East German Mark',
        'symbol' => 'DDM',
    ],
    'ECS' => [
        'code' => 'ECS',
        'name' => 'Ecuadorian Sucre',
        'symbol' => 'ECS',
    ],
    'ECV' => [
        'code' => 'ECV',
        'name' => 'Ecuadorian Unit of Constant Value',
        'symbol' => 'ECV',
    ],
    'EGP' => [
        'code' => 'EGP',
        'name' => 'Egyptian Pound',
        'symbol' => 'EGP',
    ],
    'GQE' => [
        'code' => 'GQE',
        'name' => 'Equatorial Guinean Ekwele',
        'symbol' => 'GQE',
    ],
    'ERN' => [
        'code' => 'ERN',
        'name' => 'Eritrean Nakfa',
        'symbol' => 'ERN',
    ],
    'EEK' => [
        'code' => 'EEK',
        'name' => 'Estonian Kroon',
        'symbol' => 'EEK',
    ],
    'ETB' => [
        'code' => 'ETB',
        'name' => 'Ethiopian Birr',
        'symbol' => 'ETB',
    ],
    'EUR' => [
        'code' => 'EUR',
        'name' => 'Euro',
        'symbol' => '€',
    ],
    'FKP' => [
        'code' => 'FKP',
        'name' => 'Falkland Islands Pound',
        'symbol' => 'FKP',
    ],
    'FJD' => [
        'code' => 'FJD',
        'name' => 'Fijian Dollar',
        'symbol' => 'FJD',
    ],
    'FIM' => [
        'code' => 'FIM',
        'name' => 'Finnish Markka',
        'symbol' => 'FIM',
    ],
    'FRF' => [
        'code' => 'FRF',
        'name' => 'French Franc',
        'symbol' => 'FRF',
    ],
    'XFO' => [
        'code' => 'XFO',
        'name' => 'French Gold Franc',
        'symbol' => 'XFO',
    ],
    'XFU' => [
        'code' => 'XFU',
        'name' => 'French UIC-Franc',
        'symbol' => 'XFU',
    ],
    'GMD' => [
        'code' => 'GMD',
        'name' => 'Gambian Dalasi',
        'symbol' => 'GMD',
    ],
    'GEK' => [
        'code' => 'GEK',
        'name' => 'Georgian Kupon Larit',
        'symbol' => 'GEK',
    ],
    'GEL' => [
        'code' => 'GEL',
        'name' => 'Georgian Lari',
        'symbol' => 'GEL',
    ],
    'DEM' => [
        'code' => 'DEM',
        'name' => 'German Mark',
        'symbol' => 'DEM',
    ],
    'GHS' => [
        'code' => 'GHS',
        'name' => 'Ghanaian Cedi',
        'symbol' => 'GHS',
    ],
    'GIP' => [
        'code' => 'GIP',
        'name' => 'Gibraltar Pound',
        'symbol' => 'GIP',
    ],
    'GRD' => [
        'code' => 'GRD',
        'name' => 'Greek Drachma',
        'symbol' => 'GRD',
    ],
    'GTQ' => [
        'code' => 'GTQ',
        'name' => 'Guatemalan Quetzal',
        'symbol' => 'GTQ',
    ],
    'GWP' => [
        'code' => 'GWP',
        'name' => 'Guinea-Bissau Peso',
        'symbol' => 'GWP',
    ],
    'GNF' => [
        'code' => 'GNF',
        'name' => 'Guinean Franc',
        'symbol' => 'GNF',
    ],
    'GNS' => [
        'code' => 'GNS',
        'name' => 'Guinean Syli',
        'symbol' => 'GNS',
    ],
    'GYD' => [
        'code' => 'GYD',
        'name' => 'Guyanaese Dollar',
        'symbol' => 'GYD',
    ],
    'HTG' => [
        'code' => 'HTG',
        'name' => 'Haitian Gourde',
        'symbol' => 'HTG',
    ],
    'HNL' => [
        'code' => 'HNL',
        'name' => 'Honduran Lempira',
        'symbol' => 'HNL',
    ],
    'HKD' => [
        'code' => 'HKD',
        'name' => 'Hong Kong Dollar',
        'symbol' => 'HK$',
    ],
    'HUF' => [
        'code' => 'HUF',
        'name' => 'Hungarian Forint',
        'symbol' => 'HUF',
    ],
    'ISK' => [
        'code' => 'ISK',
        'name' => 'Icelandic Króna',
        'symbol' => 'ISK',
    ],
    'INR' => [
        'code' => 'INR',
        'name' => 'Indian Rupee',
        'symbol' => '₹',
    ],
    'IDR' => [
        'code' => 'IDR',
        'name' => 'Indonesian Rupiah',
        'symbol' => 'IDR',
    ],
    'IRR' => [
        'code' => 'IRR',
        'name' => 'Iranian Rial',
        'symbol' => 'IRR',
    ],
    'IQD' => [
        'code' => 'IQD',
        'name' => 'Iraqi Dinar',
        'symbol' => 'IQD',
    ],
    'IEP' => [
        'code' => 'IEP',
        'name' => 'Irish Pound',
        'symbol' => 'IEP',
    ],
    'ILS' => [
        'code' => 'ILS',
        'name' => 'Israeli New Shekel',
        'symbol' => '₪',
    ],
    'ILP' => [
        'code' => 'ILP',
        'name' => 'Israeli Pound',
        'symbol' => 'ILP',
    ],
    'ITL' => [
        'code' => 'ITL',
        'name' => 'Italian Lira',
        'symbol' => 'ITL',
    ],
    'JMD' => [
        'code' => 'JMD',
        'name' => 'Jamaican Dollar',
        'symbol' => 'JMD',
    ],
    'JPY' => [
        'code' => 'JPY',
        'name' => 'Japanese Yen',
        'symbol' => '¥',
    ],
    'JOD' => [
        'code' => 'JOD',
        'name' => 'Jordanian Dinar',
        'symbol' => 'JOD',
    ],
    'KZT' => [
        'code' => 'KZT',
        'name' => 'Kazakhstani Tenge',
        'symbol' => 'KZT',
    ],
    'KES' => [
        'code' => 'KES',
        'name' => 'Kenyan Shilling',
        'symbol' => 'KES',
    ],
    'KWD' => [
        'code' => 'KWD',
        'name' => 'Kuwaiti Dinar',
        'symbol' => 'KWD',
    ],
    'KGS' => [
        'code' => 'KGS',
        'name' => 'Kyrgystani Som',
        'symbol' => 'KGS',
    ],
    'LAK' => [
        'code' => 'LAK',
        'name' => 'Laotian Kip',
        'symbol' => 'LAK',
    ],
    'LVL' => [
        'code' => 'LVL',
        'name' => 'Latvian Lats',
        'symbol' => 'LVL',
    ],
    'LVR' => [
        'code' => 'LVR',
        'name' => 'Latvian Ruble',
        'symbol' => 'LVR',
    ],
    'LBP' => [
        'code' => 'LBP',
        'name' => 'Lebanese Pound',
        'symbol' => 'LBP',
    ],
    'LSL' => [
        'code' => 'LSL',
        'name' => 'Lesotho Loti',
        'symbol' => 'LSL',
    ],
    'LRD' => [
        'code' => 'LRD',
        'name' => 'Liberian Dollar',
        'symbol' => 'LRD',
    ],
    'LYD' => [
        'code' => 'LYD',
        'name' => 'Libyan Dinar',
        'symbol' => 'LYD',
    ],
    'LTL' => [
        'code' => 'LTL',
        'name' => 'Lithuanian Litas',
        'symbol' => 'LTL',
    ],
    'LTT' => [
        'code' => 'LTT',
        'name' => 'Lithuanian Talonas',
        'symbol' => 'LTT',
    ],
    'LUL' => [
        'code' => 'LUL',
        'name' => 'Luxembourg Financial Franc',
        'symbol' => 'LUL',
    ],
    'LUC' => [
        'code' => 'LUC',
        'name' => 'Luxembourgian Convertible Franc',
        'symbol' => 'LUC',
    ],
    'LUF' => [
        'code' => 'LUF',
        'name' => 'Luxembourgian Franc',
        'symbol' => 'LUF',
    ],
    'MOP' => [
        'code' => 'MOP',
        'name' => 'Macanese Pataca',
        'symbol' => 'MOP',
    ],
    'MKD' => [
        'code' => 'MKD',
        'name' => 'Macedonian Denar',
        'symbol' => 'MKD',
    ],
    'MGA' => [
        'code' => 'MGA',
        'name' => 'Malagasy Ariary',
        'symbol' => 'MGA',
    ],
    'MGF' => [
        'code' => 'MGF',
        'name' => 'Malagasy Franc',
        'symbol' => 'MGF',
    ],
    'MWK' => [
        'code' => 'MWK',
        'name' => 'Malawian Kwacha',
        'symbol' => 'MWK',
    ],
    'MYR' => [
        'code' => 'MYR',
        'name' => 'Malaysian Ringgit',
        'symbol' => 'MYR',
    ],
    'MVR' => [
        'code' => 'MVR',
        'name' => 'Maldivian Rufiyaa',
        'symbol' => 'MVR',
    ],
    'MLF' => [
        'code' => 'MLF',
        'name' => 'Malian Franc',
        'symbol' => 'MLF',
    ],
    'MTL' => [
        'code' => 'MTL',
        'name' => 'Maltese Lira',
        'symbol' => 'MTL',
    ],
    'MTP' => [
        'code' => 'MTP',
        'name' => 'Maltese Pound',
        'symbol' => 'MTP',
    ],
    'MRU' => [
        'code' => 'MRU',
        'name' => 'Mauritanian Ouguiya',
        'symbol' => 'MRU',
    ],
    'MUR' => [
        'code' => 'MUR',
        'name' => 'Mauritian Rupee',
        'symbol' => 'MUR',
    ],
    'MXV' => [
        'code' => 'MXV',
        'name' => 'Mexican Investment Unit',
        'symbol' => 'MXV',
    ],
    'MXN' => [
        'code' => 'MXN',
        'name' => 'Mexican Peso',
        'symbol' => 'MX$',
    ],
    'MDC' => [
        'code' => 'MDC',
        'name' => 'Moldovan Cupon',
        'symbol' => 'MDC',
    ],
    'MDL' => [
        'code' => 'MDL',
        'name' => 'Moldovan Leu',
        'symbol' => 'MDL',
    ],
    'MCF' => [
        'code' => 'MCF',
        'name' => 'Monegasque Franc',
        'symbol' => 'MCF',
    ],
    'MNT' => [
        'code' => 'MNT',
        'name' => 'Mongolian Tugrik',
        'symbol' => 'MNT',
    ],
    'MAD' => [
        'code' => 'MAD',
        'name' => 'Moroccan Dirham',
        'symbol' => 'MAD',
    ],
    'MAF' => [
        'code' => 'MAF',
        'name' => 'Moroccan Franc',
        'symbol' => 'MAF',
    ],
    'MZE' => [
        'code' => 'MZE',
        'name' => 'Mozambican Escudo',
        'symbol' => 'MZE',
    ],
    'MZN' => [
        'code' => 'MZN',
        'name' => 'Mozambican Metical',
        'symbol' => 'MZN',
    ],
    'MMK' => [
        'code' => 'MMK',
        'name' => 'Myanmar Kyat',
        'symbol' => 'MMK',
    ],
    'NAD' => [
        'code' => 'NAD',
        'name' => 'Namibian Dollar',
        'symbol' => 'NAD',
    ],
    'NPR' => [
        'code' => 'NPR',
        'name' => 'Nepalese Rupee',
        'symbol' => 'NPR',
    ],
    'ANG' => [
        'code' => 'ANG',
        'name' => 'Netherlands Antillean Guilder',
        'symbol' => 'ANG',
    ],
    'TWD' => [
        'code' => 'TWD',
        'name' => 'New Taiwan Dollar',
        'symbol' => 'NT$',
    ],
    'NZD' => [
        'code' => 'NZD',
        'name' => 'New Zealand Dollar',
        'symbol' => 'NZ$',
    ],
    'NIO' => [
        'code' => 'NIO',
        'name' => 'Nicaraguan Córdoba',
        'symbol' => 'NIO',
    ],
    'NGN' => [
        'code' => 'NGN',
        'name' => 'Nigerian Naira',
        'symbol' => 'NGN',
    ],
    'KPW' => [
        'code' => 'KPW',
        'name' => 'North Korean Won',
        'symbol' => 'KPW',
    ],
    'NOK' => [
        'code' => 'NOK',
        'name' => 'Norwegian Krone',
        'symbol' => 'NOK',
    ],
    'OMR' => [
        'code' => 'OMR',
        'name' => 'Omani Rial',
        'symbol' => 'OMR',
    ],
    'PKR' => [
        'code' => 'PKR',
        'name' => 'Pakistani Rupee',
        'symbol' => 'PKR',
    ],
    'PAB' => [
        'code' => 'PAB',
        'name' => 'Panamanian Balboa',
        'symbol' => 'PAB',
    ],
    'PGK' => [
        'code' => 'PGK',
        'name' => 'Papua New Guinean Kina',
        'symbol' => 'PGK',
    ],
    'PYG' => [
        'code' => 'PYG',
        'name' => 'Paraguayan Guarani',
        'symbol' => 'PYG',
    ],
    'PEI' => [
        'code' => 'PEI',
        'name' => 'Peruvian Inti',
        'symbol' => 'PEI',
    ],
    'PEN' => [
        'code' => 'PEN',
        'name' => 'Peruvian Sol',
        'symbol' => 'PEN',
    ],
    'PHP' => [
        'code' => 'PHP',
        'name' => 'Philippine Peso',
        'symbol' => '₱',
    ],
    'PLN' => [
        'code' => 'PLN',
        'name' => 'Polish Zloty',
        'symbol' => 'PLN',
    ],
    'PTE' => [
        'code' => 'PTE',
        'name' => 'Portuguese Escudo',
        'symbol' => 'PTE',
    ],
    'GWE' => [
        'code' => 'GWE',
        'name' => 'Portuguese Guinea Escudo',
        'symbol' => 'GWE',
    ],
    'QAR' => [
        'code' => 'QAR',
        'name' => 'Qatari Riyal',
        'symbol' => 'QAR',
    ],
    'RHD' => [
        'code' => 'RHD',
        'name' => 'Rhodesian Dollar',
        'symbol' => 'RHD',
    ],
    'XRE' => [
        'code' => 'XRE',
        'name' => 'RINET Funds',
        'symbol' => 'XRE',
    ],
    'RON' => [
        'code' => 'RON',
        'name' => 'Romanian Leu',
        'symbol' => 'RON',
    ],
    'RUB' => [
        'code' => 'RUB',
        'name' => 'Russian Ruble',
        'symbol' => '₽',
    ],
    'RWF' => [
        'code' => 'RWF',
        'name' => 'Rwandan Franc',
        'symbol' => 'RWF',
    ],
    'SVC' => [
        'code' => 'SVC',
        'name' => 'Salvadoran Colón',
        'symbol' => 'SVC',
    ],
    'WST' => [
        'code' => 'WST',
        'name' => 'Samoan Tala',
        'symbol' => 'WST',
    ],
    'STN' => [
        'code' => 'STN',
        'name' => 'São Tomé & Príncipe Dobra',
        'symbol' => 'STN',
    ],
    'SAR' => [
        'code' => 'SAR',
        'name' => 'Saudi Riyal',
        'symbol' => 'SAR',
    ],
    'RSD' => [
        'code' => 'RSD',
        'name' => 'Serbian Dinar',
        'symbol' => 'RSD',
    ],
    'SCR' => [
        'code' => 'SCR',
        'name' => 'Seychellois Rupee',
        'symbol' => 'SCR',
    ],
    'SLE' => [
        'code' => 'SLE',
        'name' => 'Sierra Leonean Leone',
        'symbol' => 'SLE',
    ],
    'SGD' => [
        'code' => 'SGD',
        'name' => 'Singapore Dollar',
        'symbol' => 'SGD',
    ],
    'SKK' => [
        'code' => 'SKK',
        'name' => 'Slovak Koruna',
        'symbol' => 'SKK',
    ],
    'SIT' => [
        'code' => 'SIT',
        'name' => 'Slovenian Tolar',
        'symbol' => 'SIT',
    ],
    'SBD' => [
        'code' => 'SBD',
        'name' => 'Solomon Islands Dollar',
        'symbol' => 'SBD',
    ],
    'SOS' => [
        'code' => 'SOS',
        'name' => 'Somali Shilling',
        'symbol' => 'SOS',
    ],
    'ZAR' => [
        'code' => 'ZAR',
        'name' => 'South African Rand',
        'symbol' => 'ZAR',
    ],
    'ZAL' => [
        'code' => 'ZAL',
        'name' => 'South African Rand (financial)',
        'symbol' => 'ZAL',
    ],
    'KRW' => [
        'code' => 'KRW',
        'name' => 'South Korean Won',
        'symbol' => '₩',
    ],
    'SSP' => [
        'code' => 'SSP',
        'name' => 'South Sudanese Pound',
        'symbol' => 'SSP',
    ],
    'SUR' => [
        'code' => 'SUR',
        'name' => 'Soviet Rouble',
        'symbol' => 'SUR',
    ],
    'ESP' => [
        'code' => 'ESP',
        'name' => 'Spanish Peseta',
        'symbol' => 'ESP',
    ],
    'ESA' => [
        'code' => 'ESA',
        'name' => 'Spanish Peseta (A account)',
        'symbol' => 'ESA',
    ],
    'ESB' => [
        'code' => 'ESB',
        'name' => 'Spanish Peseta (convertible account)',
        'symbol' => 'ESB',
    ],
    'LKR' => [
        'code' => 'LKR',
        'name' => 'Sri Lankan Rupee',
        'symbol' => 'LKR',
    ],
    'SHP' => [
        'code' => 'SHP',
        'name' => 'St. Helena Pound',
        'symbol' => 'SHP',
    ],
    'SDG' => [
        'code' => 'SDG',
        'name' => 'Sudanese Pound',
        'symbol' => 'SDG',
    ],
    'SRD' => [
        'code' => 'SRD',
        'name' => 'Surinamese Dollar',
        'symbol' => 'SRD',
    ],
    'SRG' => [
        'code' => 'SRG',
        'name' => 'Surinamese Guilder',
        'symbol' => 'SRG',
    ],
    'SZL' => [
        'code' => 'SZL',
        'name' => 'Swazi Lilangeni',
        'symbol' => 'SZL',
    ],
    'SEK' => [
        'code' => 'SEK',
        'name' => 'Swedish Krona',
        'symbol' => 'SEK',
    ],
    'CHF' => [
        'code' => 'CHF',
        'name' => 'Swiss Franc',
        'symbol' => '₣',
    ],
    'SYP' => [
        'code' => 'SYP',
        'name' => 'Syrian Pound',
        'symbol' => 'SYP',
    ],
    'TJR' => [
        'code' => 'TJR',
        'name' => 'Tajikistani Ruble',
        'symbol' => 'TJR',
    ],
    'TJS' => [
        'code' => 'TJS',
        'name' => 'Tajikistani Somoni',
        'symbol' => 'TJS',
    ],
    'TZS' => [
        'code' => 'TZS',
        'name' => 'Tanzanian Shilling',
        'symbol' => 'TZS',
    ],
    'THB' => [
        'code' => 'THB',
        'name' => 'Thai Baht',
        'symbol' => 'THB',
    ],
    'TPE' => [
        'code' => 'TPE',
        'name' => 'Timorese Escudo',
        'symbol' => 'TPE',
    ],
    'TOP' => [
        'code' => 'TOP',
        'name' => 'Tongan Paʻanga',
        'symbol' => 'TOP',
    ],
    'TTD' => [
        'code' => 'TTD',
        'name' => 'Trinidad & Tobago Dollar',
        'symbol' => 'TTD',
    ],
    'TND' => [
        'code' => 'TND',
        'name' => 'Tunisian Dinar',
        'symbol' => 'TND',
    ],
    'TRY' => [
        'code' => 'TRY',
        'name' => 'Turkish Lira',
        'symbol' => '₺',
    ],
    'TMT' => [
        'code' => 'TMT',
        'name' => 'Turkmenistani Manat',
        'symbol' => 'TMT',
    ],
    'UGX' => [
        'code' => 'UGX',
        'name' => 'Ugandan Shilling',
        'symbol' => 'UGX',
    ],
    'UAH' => [
        'code' => 'UAH',
        'name' => 'Ukrainian Hryvnia',
        'symbol' => 'UAH',
    ],
    'UAK' => [
        'code' => 'UAK',
        'name' => 'Ukrainian Karbovanets',
        'symbol' => 'UAK',
    ],
    'AED' => [
        'code' => 'AED',
        'name' => 'United Arab Emirates Dirham',
        'symbol' => 'AED',
    ],
    'UYW' => [
        'code' => 'UYW',
        'name' => 'Uruguayan Nominal Wage Index Unit',
        'symbol' => 'UYW',
    ],
    'UYU' => [
        'code' => 'UYU',
        'name' => 'Uruguayan Peso',
        'symbol' => 'UYU',
    ],
    'UYI' => [
        'code' => 'UYI',
        'name' => 'Uruguayan Peso (Indexed Units)',
        'symbol' => 'UYI',
    ],
    'USD' => [
        'code' => 'USD',
        'name' => 'US Dollar',
        'symbol' => '$',
    ],
    'USN' => [
        'code' => 'USN',
        'name' => 'US Dollar (Next day)',
        'symbol' => 'USN',
    ],
    'USS' => [
        'code' => 'USS',
        'name' => 'US Dollar (Same day)',
        'symbol' => 'USS',
    ],
    'UZS' => [
        'code' => 'UZS',
        'name' => 'Uzbekistani Som',
        'symbol' => 'UZS',
    ],
    'VUV' => [
        'code' => 'VUV',
        'name' => 'Vanuatu Vatu',
        'symbol' => 'VUV',
    ],
    'VES' => [
        'code' => 'VES',
        'name' => 'Venezuelan Bolívar',
        'symbol' => 'VES',
    ],
    'VND' => [
        'code' => 'VND',
        'name' => 'Vietnamese Dong',
        'symbol' => '₫',
    ],
    'XOF' => [
        'code' => 'XOF',
        'name' => 'West African CFA Franc',
        'symbol' => 'F CFA',
    ],
    'CHE' => [
        'code' => 'CHE',
        'name' => 'WIR Euro',
        'symbol' => 'CHE',
    ],
    'CHW' => [
        'code' => 'CHW',
        'name' => 'WIR Franc',
        'symbol' => 'CHW',
    ],
    'YDD' => [
        'code' => 'YDD',
        'name' => 'Yemeni Dinar',
        'symbol' => 'YDD',
    ],
    'YER' => [
        'code' => 'YER',
        'name' => 'Yemeni Rial',
        'symbol' => 'YER',
    ],
    'ZMW' => [
        'code' => 'ZMW',
        'name' => 'Zambian Kwacha',
        'symbol' => 'ZMW',
    ],
];
