<?php

namespace Tests;

use Compass\Utils\StringUtils;

abstract class UtilityTestCase extends \PHPUnit\Framework\TestCase
{
    public function testCanNotInstantiate()
    {
        $this->expectException(\Error::class);
        $this->expectExceptionMessage(
            'Call to private Compass\Utils\AbstractUtils::__construct() from scope Tests\UtilityTestCase'
        );

        $class = $this->getUtilityClass();

        new $class();
    }

    protected function getUtilityClass(): string
    {
        $class = StringUtils::removeSuffix(get_called_class(), 'Test');

        return str_replace('Tests\\', 'Compass\\Utils\\', $class);
    }
}