<?php

namespace Tests;

use Compass\Utils\UrlUtils;

class UrlUtilsTest extends UtilityTestCase
{

    /**
     * @dataProvider provideGetQueryParamsData
     */
    public function testGetQueryParams(array $expected, string $url)
    {
        $this->assertEquals($expected, UrlUtils::getQueryParams($url));
    }

    public function provideGetQueryParamsData(): \Generator
    {
        yield 'empty params' => [
            [],
            '/path/to/url',
        ];
        yield 'page query params' => [
            ['page' => '100'],
            '/path/to/url?page=100',
        ];
        yield 'page and perPage query params' => [
            ['page' => '100', 'perPage' => '10'],
            '/path/to/url?page=100&perPage=10',
        ];
    }

    /**
     * @dataProvider provideAddQueryParamsData
     */
    public function testAddQueryParams(string $expected, string $url, array $params)
    {
        $this->assertEquals($expected, UrlUtils::addQueryParams($url, $params));
    }

    public static function provideAddQueryParamsData(): \Generator
    {
        yield 'simple' => [
            'https://example.com/?foo=bar',
            'https://example.com',
            ['foo' => 'bar'],
        ];
        yield 'empty url' => [
            '/?foo=bar',
            '',
            ['foo' => 'bar'],
        ];
        yield 'add more param' => [
            'https://example.com/?foo=bar&baz=qux',
            'https://example.com/?foo=bar',
            ['baz' => 'qux'],
        ];
        yield 'url with port number' => [
            'http://:80/?foo=bar',
            'http://:80',
            ['foo' => 'bar'],
        ];
        yield 'url with port number and user' => [
            'http://user@:80/?foo=bar',
            'http://user@:80',
            ['foo' => 'bar'],
        ];
    }

    /**
     * @dataProvider provideAddQueryParamData
     */
    public function testAddQueryParam(string $expected, string $url, string $param, mixed $value)
    {
        $this->assertEquals($expected, UrlUtils::addQueryParam($url, $param, $value));
    }

    public function provideAddQueryParamData(): \Generator
    {
        yield 'simple' => [
            'https://example.com/?foo=bar',
            'https://example.com',
            'foo', 'bar',
        ];
        yield 'empty url' => [
            '/?foo=bar',
            '',
            'foo', 'bar',
        ];
        yield 'add more param' => [
            'https://example.com/?foo=bar&baz=qux',
            'https://example.com/?foo=bar',
            'baz', 'qux',
        ];
        yield 'url with port number' => [
            'http://:80/?foo=bar',
            'http://:80',
            'foo', 'bar',
        ];
        yield 'url with port number and user' => [
            'http://user@:80/?foo=bar',
            'http://user@:80',
            'foo', 'bar',
        ];
    }

    /**
     * @dataProvider provideRemoveQueryParamData
     */
    public function testRemoveQueryParam(string $expected, string $url, string|array $queryParams)
    {
        $this->assertEquals($expected, UrlUtils::removeQueryParam($url, $queryParams));
    }

    public function provideRemoveQueryParamData(): \Generator
    {
        yield [
            '/path/to/url',
            '/path/to/url?page=100',
            'page',
        ];
        yield [
            '/path/to/url?perPage=10&embed=foo,bar,baz&sort=-qux&field=name,status,qux',
            '/path/to/url?perPage=10&embed=foo,bar,baz&sort=-qux&field=name,status,qux&page=2',
            'page',
        ];
        yield [
            '/path/to/url?perPage=10&embed=foo,bar,baz&sort=-qux&field=name,status,qux',
            '/path/to/url?page=2&perPage=10&embed=foo,bar,baz&sort=-qux&field=name,status,qux',
            'page',
        ];
    }

    /**
     * @dataProvider provideModifyQueryParamData
     */
    public function testModifyQueryParam(string $expected, string $url, string $param, mixed $value)
    {
        $this->assertEquals($expected, UrlUtils::modifyQueryParam($url, $param, $value));
    }

    public function provideModifyQueryParamData(): \Generator
    {
        yield ['/path/to/url?page=99', '/path/to/url', 'page', 99];
        yield ['/path/to/url?page=99', '/path/to/url?page=100', 'page', 99];
        yield ['/path/to/url?page=99&perPage=20', '/path/to/url?page=99&perPage=10', 'perPage', 20];
    }

    /**
     * @dataProvider provideGenerateUrlData
     */
    public function testGenerateUrl(string $expected, string $url, array $attributes)
    {
        $this->assertEquals($expected, UrlUtils::generateUrl($url, $attributes));
    }

    public static function provideGenerateUrlData(): \Generator
    {
        yield 'empty attributes' => [
            '/path/to/url',
            '/path/to/url',
            []
        ];
        yield 'simple attributes' => [
            '/path/to/url/999',
            '/path/to/url/{id}',
            ['id' => 999],
        ];
        yield 'multiple attributes' => [
            '/path/99/to/url/john',
            '/path/{id}/to/url/{name}',
            ['id' => 99, 'name' => 'john'],
        ];
        yield 'multiple same attributes' => [
            '/path/99/to/url/99',
            '/path/{id}/to/url/{id}',
            ['id' => 99],
        ];
        yield 'add query param' => [
            '/path/99/to/url?name=john',
            '/path/{id}/to/url',
            ['id' => 99, 'name' => 'john'],
        ];
        yield 'add query param when exists' => [
            '/path/99/to/url?name=john&age=18',
            '/path/{id}/to/url?name=john',
            ['id' => 99, 'age' => 18],
        ];
        yield 'url with query string' => [
            '/path/99/to/url/john/?foo=bar&baz=qux',
            '/path/{id}/to/url/{name}/?foo=bar&baz=qux',
            ['id' => 99, 'name' => 'john'],
        ];
    }

    public function testGenerateUrlThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The attributes "name" do not match the provided parameters.');

        UrlUtils::generateUrl('/path/{id}/to/url/{name}', ['id' => 'bar']);
    }
}