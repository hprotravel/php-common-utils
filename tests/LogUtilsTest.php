<?php

namespace Tests;

use Compass\Utils\LogUtils;

class LogUtilsTest extends UtilityTestCase
{
    public function testFormat()
    {
        $exception = new \Exception('Test exception');

        $log = LogUtils::format($exception);

        $this->assertArrayHasKey('code', $log);
        $this->assertArrayHasKey('message', $log);
        $this->assertArrayHasKey('called', $log);
        $this->assertArrayHasKey('occurred', $log);
    }

    public function testFormatWithPrevious()
    {
        $exception = new \Exception('Test exception', 0, new \Exception('Previous exception'));

        $log = LogUtils::format($exception);

        $this->assertArrayHasKey('previous', $log);
    }
}