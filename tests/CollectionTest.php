<?php

namespace Tests;

use Compass\Utils\Collection;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    public function testSort()
    {
        $collection = new Collection(['foo' => true, 'bar' => 'baz']);

        $sortedCollection = $collection->sort();

        $this->assertSame(['foo' => true, 'bar' => 'baz'], $sortedCollection->all());

        $sortedCollection = $collection->sort(
            function ($a, $b) {
                return $a <=> $b;
            }
        );

        $this->assertSame(['foo' => true, 'bar' => 'baz'], $sortedCollection->all());
    }

    public function testSortKeys()
    {
        $collection = new Collection(['foo' => true, 'bar' => 'baz']);

        $sortedCollection = $collection->sortKeys();

        $this->assertSame(['bar' => 'baz', 'foo' => true], $sortedCollection->all());
    }

    public function testRange()
    {
        $rangeCollection = (new Collection())->range(1, 9);

        $this->assertSame([1, 2, 3, 4, 5, 6, 7, 8, 9], $rangeCollection->all());
    }

    public function testAll()
    {
        $collection = new Collection(['foo' => true, 'bar' => 'baz']);

        $this->assertSame(['foo' => true, 'bar' => 'baz'], $collection->all());
    }

    public function testKeys()
    {
        $collection = new Collection(['foo' => true, 'bar' => 'baz']);

        $keysCollection = $collection->keys();

        $this->assertSame(['foo', 'bar'], $keysCollection->all());
    }

    public function testContains()
    {
        $collection = new Collection(['foo', 'bar', 'baz']);

        $this->assertTrue($collection->contains('foo'));
        $this->assertNotTrue($collection->contains('invalid'));
    }

    public function testSearch()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame('bar', $collection->search('baz'));

        $collection = new Collection(['foo' => true, 'bar' => 'baz']);

        $this->assertSame('foo', $collection->search('baz'));
        $this->assertSame('bar', $collection->search('baz', true));
    }

    public function testExcept()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame([], $collection->except(['foo', 'bar'])->all());
        $this->assertSame(['foo' => 12], $collection->except('bar')->all());
    }

    public function testOnly()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame(['foo' => 12, 'bar' => 'baz'], $collection->only(['foo', 'bar'])->all());
        $this->assertSame(['foo' => 12], $collection->only('foo')->all());
    }

    public function testFirst()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame(12, $collection->first());
    }

    public function testLast()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame('baz', $collection->last());
    }

    public function testGet()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertSame('baz', $collection->get('bar'));
    }

    public function testHas()
    {
        $collection = new Collection(['foo' => 12, 'bar' => 'baz']);

        $this->assertTrue($collection->has('foo'));
        $this->assertFalse($collection->has('baz'));
    }

    public function testMap()
    {
        $collection = new Collection([1, 2]);

        $this->assertSame([2, 4], $collection->map(fn($value) => $value * 2)->all());
    }

    public function testFilter()
    {
        $collection = new Collection([1, 2, 3, 4, 5, 6, 7]);

        $this->assertSame([4, 5, 6, 7], $collection->filter(fn($value) => $value > 3)->all(false));
    }

    public function testCount()
    {
        $collection = new Collection([1, 2, 3, 4, 5, 6, 7]);

        $this->assertEquals(7, $collection->count());
    }

    public function testJoin()
    {
        $collection = new Collection([1, 2, 3, 4, 5, 6, 7]);

        $this->assertSame('1,2,3,4,5,6,7', $collection->join());
    }

    // TODO: add more tests
}
