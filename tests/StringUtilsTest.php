<?php

namespace Tests;

use Compass\Utils\StringUtils;

class StringUtilsTest extends UtilityTestCase
{
    public function testEmpty()
    {
        $this->assertTrue(StringUtils::empty(''));
        $this->assertTrue(StringUtils::empty(' ', true));
        $this->assertTrue(StringUtils::empty('  ', true));

        $this->assertFalse(StringUtils::empty('  '));
        $this->assertFalse(StringUtils::empty('0'));
        $this->assertFalse(StringUtils::empty('false'));
        $this->assertFalse(StringUtils::empty('1'));
        $this->assertFalse(StringUtils::empty('true'));
    }

    public function testRandom()
    {
        $this->assertEquals(8, strlen(StringUtils::random()));
        $this->assertEquals(40, strlen(StringUtils::random(40)));
        $this->assertNotEquals(StringUtils::random(), StringUtils::random());
        $this->assertNotEquals(str_random(), str_random());
    }

    /**
     * @dataProvider provideStartsWithData
     */
    public function testStartsWith(bool $expected, string $haystack, $needles)
    {
        $this->assertEquals($expected, StringUtils::startsWith($haystack, $needles));
    }

    public function provideStartsWithData(): \Generator
    {
        yield 'all strings start with the empty string' => [
            true,
            'All strings start with the empty string',
            '',
        ];
        yield 'string start with true' => [true, 'The lazy fox jumped over the fence', 'The'];
        yield 'string start with false, case sensitive check' => [false, 'The lazy fox jumped over the fence', 'the'];
    }

    /**
     * @dataProvider provideEndsWithData
     */
    public function testEndsWith(bool $expected, string $haystack, $needles)
    {
        $this->assertEquals($expected, StringUtils::endsWith($haystack, $needles));
    }

    public function provideEndsWithData(): \Generator
    {
        yield 'all strings end with the empty string' => [
            true,
            'All strings end with the empty string',
            '',
        ];
        yield 'string end with true' => [true, 'The lazy fox jumped over the fence', 'fence'];
        yield 'string end with false, case sensitive check' => [false, 'The lazy fox jumped over the fence', 'Fence'];
    }

    /**
     * @dataProvider provideContainsData
     */
    public function testContains(bool $expected, string $haystack, $needles)
    {
        $this->assertEquals($expected, StringUtils::contains($haystack, $needles));
    }

    public function provideContainsData(): \Generator
    {
        yield 'all strings contain the empty string' => [
            true,
            'Checking the existence of the empty string will always return true',
            '',
        ];
        yield 'string contain true' => [true, 'The lazy fox jumped over the fence', 'lazy'];
        yield 'string contain false, case sensitive check' => [false, 'The lazy fox jumped over the fence', 'Lazy'];
    }

    /**
     * @dataProvider provideSlugData
     */
    public function testSlug(string $expected, string $input, string $separator = '-')
    {
        $this->assertEquals($expected, StringUtils::slug($input, $separator));
    }

    public function provideSlugData(): \Generator
    {
        yield [
            '0123456789-abcdefghijklmnopqrstuvwxyz-abcdefghijklmnopqrstuvwxyz',
            '0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz',
        ];
        yield ['a_b_v', ' A B   V ', '_'];
    }

    /**
     * @dataProvider provideLimitData
     */
    public function testLimit(string $expected, string $input, int $limit)
    {
        $this->assertEquals($expected, StringUtils::limit($input, $limit));
    }

    public function provideLimitData(): \Generator
    {
        yield ['Foo...', 'Foo Bar', 3];
        yield [
            'Sed ut perspiciatis, unde omnis iste natus error s...',
            'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
            rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, 
            explicabo.',
            50,
        ];
    }

    /**
     * @dataProvider provideInterpolateData
     */
    public function testInterpolate(string $expected, string $input, array $context)
    {
        $this->assertEquals($expected, StringUtils::interpolate($input, $context));
    }

    public function provideInterpolateData(): \Generator
    {
        yield 'interpolate single context item' => [
            'User bolivar created',
            'User {username} created',
            ['username' => 'bolivar'],
        ];
        yield 'interpolate doubled context item' => [
            'User bolivar created for bolivar',
            'User {username} created for {username}',
            ['username' => 'bolivar'],
        ];
    }

    /**
     * @dataProvider provideStripQuotesData
     */
    public function testStripQuotes(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::stripQuotes($input));
    }

    public function provideStripQuotesData(): \Generator
    {
        yield 'strip double quotes' => ['foo', '"foo"'];
        yield 'strip single quotes' => ['foo', "'foo'"];
    }

    /**
     * @dataProvider provideLowerCaseData
     */
    public function testLowerCase(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::lowerCase($input));
    }

    public function provideLowerCaseData(): \Generator
    {
        yield ['foo bar', 'FOO BAR'];
        yield ['foo bar', 'Foo bar'];
        yield ['foo bar', 'foo Bar'];
    }

    /**
     * @dataProvider provideUpperCaseData
     */
    public function testUpperCase(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::upperCase($input));
    }

    public function provideUpperCaseData(): \Generator
    {
        yield ['FOO BAR', 'foo bar'];
        yield ['FOO BAR', 'Foo bar'];
        yield ['FOO BAR', 'foo Bar'];
    }

    /**
     * @dataProvider provideTitleCaseData
     */
    public function testTitleCase(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::titleCase($input));
    }

    public function provideTitleCaseData(): \Generator
    {
        yield ['Foo Bar', 'foo bar'];
        yield ['Foo Bar', 'Foo bar'];
        yield ['Foo Bar', 'foo Bar'];
    }

    /**
     * @dataProvider provideSnakeCaseData
     */
    public function testSnakeCase(string $expected, string $input, ?string $delimiter = null)
    {
        $this->assertEquals(
            $expected,
            $delimiter
                ? StringUtils::snakeCase($input, $delimiter)
                : StringUtils::snakeCase($input)
        );
    }

    public function provideSnakeCaseData(): \Generator
    {
        yield ['foo_bar', 'FooBar'];
        yield ['foo_bar', 'fooBar'];
        yield ['foo_bar', 'foo Bar'];
        yield ['foo_bar', 'foo bar'];
        yield ['foo-bar', 'foo bar', '-'];
        yield ['foo|bar', 'foo bar', '|'];
    }

    /**
     * @dataProvider provideCamelCaseData
     */
    public function testCamelCase(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::camelCase($input));
    }

    public function provideCamelCaseData(): \Generator
    {
        yield ['fooBar', 'foo_bar'];
        yield ['fooBar', 'FooBar'];
        yield ['fooBar', 'Foo_Bar'];
        yield ['fooBar', 'Foo Bar'];
        yield ['fooBar', 'foo bar'];
    }

    /**
     * @dataProvider provideStudlyCaseData
     */
    public function testStudlyCase(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::studlyCase($input));
    }

    public function provideStudlyCaseData(): \Generator
    {
        yield ['FooBar', 'foo_bar'];
        yield ['FooBar', 'FooBar'];
        yield ['FooBar', 'Foo_Bar'];
        yield ['FooBar', 'Foo Bar'];
        yield ['FooBar', 'foo bar'];
    }

    /**
     * @dataProvider provideHeadlineData
     */
    public function testHeadline(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::headline($input));
    }

    public function testUcsplit()
    {
        $this->assertEquals(['foo', 'Bar'], StringUtils::ucsplit('fooBar'));
    }

    public function provideHeadlineData(): \Generator
    {
        yield ['Foo Bar', 'FooBar'];
        yield ['Foo Bar', 'fooBar'];
        yield ['Foo Bar', 'foo_bar'];
    }

    /**
     * @dataProvider provideUcfirstData
     */
    public function testUcfirst(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::ucfirst($input));
    }

    public function provideUcfirstData(): \Generator
    {
        yield ['Foo_bar', 'foo_bar'];
        yield ['FooBar', 'FooBar'];
        yield ['FooBar', 'fooBar'];
        yield ['Foo_Bar', 'Foo_Bar'];
        yield ['Foo Bar', 'Foo Bar'];
        yield ['Foo bar', 'foo bar'];
    }

    /**
     * @dataProvider provideReplaceData
     */
    public function testReplace(
        iterable|string $expected,
        iterable|string $search,
        iterable|string $replace,
        array|string $input
    ) {
        $this->assertEquals($expected, StringUtils::replace($search, $replace, $input));
    }

    public function provideReplaceData(): \Generator
    {
        yield ['Foo baz', 'bar', 'baz', 'Foo bar'];
        yield ['Foo baz', ['bar'], ['baz'], 'Foo bar'];
        yield ['Foo baz baz', ['bar', 'buz'], ['baz', 'baz'], 'Foo bar buz'];
        yield [['Foo baz baz'], ['bar', 'buz'], ['baz', 'baz'], ['Foo bar buz']];
    }

    /**
     * @dataProvider provideReplaceCharWithData
     */
    public function testReplaceCharsWith(string $expected, $input, int $first, int $last, string $replace = '*')
    {
        $this->assertEquals($expected, StringUtils::replaceCharWith($input, $first, $last, $replace));
    }

    public function provideReplaceCharWithData(): \Generator
    {
        yield 'replace it from the center' => ['ChangeMy******Code', 'ChangeMySecretCode', 8, 14];
        yield 'replace it from the beginning' => ['******MySecretCode', 'ChangeMySecretCode', 0, 6];
        yield 'replace it with custom replacement' => ['Change##SecretCode', 'ChangeMySecretCode', 6, 8, '#'];
        yield 'replace it from the end' => ['ChangeMySecret****', 'ChangeMySecretCode', -4, 0];
        yield 'replace it from the center by reference end' => ['ChangeMy******Code', 'ChangeMySecretCode', -10, -4];
        yield 'replace it from the beginning with non string' => ['***456789', 123456789, 0, 3];
    }

    /**
     * @dataProvider provideRemoveFeedLineData
     */
    public function testRemoveFeedLine(string $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::removeLineFeed($input));
    }

    public function provideRemoveFeedLineData(): \Generator
    {
        yield ['Foo Bar', "Foo Bar"];
        yield ['Foo Bar', "Foo Bar\n"];
        yield ['Foo Bar', "Foo Bar\r"];
        yield ['Foo Bar', "Foo Bar\r\n"];
    }

    /**
     * @dataProvider provideRemoveSuffixData
     */
    public function testRemoveSuffix(string $expected, string $input, string $remove)
    {
        $this->assertEquals($expected, StringUtils::removeSuffix($input, $remove));
    }

    public function provideRemoveSuffixData(): \Generator
    {
        yield ['Foo', 'FooBar', 'Bar'];
        yield ['FooBar', 'FooBar', 'bar'];
    }

    /**
     * @dataProvider provideRemovePrefixData
     */
    public function testRemovePrefix(string $expected, string $input, string $remove)
    {
        $this->assertEquals($expected, StringUtils::removePrefix($input, $remove));
    }

    public function provideRemovePrefixData(): \Generator
    {
        yield ['Bar', 'FooBar', 'Foo'];
        yield ['FooBar', 'FooBar', 'foo'];
    }

    public function testHash()
    {
        $this->assertEquals(32, strlen(StringUtils::hash(new \stdClass())));
        $this->assertEquals(40, strlen(StringUtils::hash([1, 'foo', '125'], 'sha1')));
        $this->assertEquals(8, strlen(StringUtils::hash('foo', 'crc32')));
    }

    /**
     * @dataProvider provideSplitData
     */
    public function testSplit(array $expected, string $input)
    {
        $this->assertEquals($expected, StringUtils::split($input));
    }

    /**
     * @dataProvider provideEscapeData
     */
    public function testEscape(string $input, string $output)
    {
        $this->assertEquals($output, StringUtils::escape($input));
    }

    public static function provideEscapeData(): \Generator
    {
        yield 'substitute' => [
            '<script></script>',
            '&lt;script&gt;&lt;/script&gt;'
        ];
        yield 'single quotes' => [
            '<script>alert(\'hello world!\');</script>',
            '&lt;script&gt;alert(&#039;hello world!&#039;);&lt;/script&gt;'
        ];
        yield 'double quotes' => [
            '<script>alert("hello world!");</script>',
            '&lt;script&gt;alert(&quot;hello world!&quot;);&lt;/script&gt;'
        ];
    }

    public static function provideSplitData(): \Generator
    {
        yield 'comma seperated' => [
            ['foo', 'bar', 'baz'],
            'foo,bar,baz',
        ];
        yield 'colon seperated' => [
            ['foo', 'bar', 'baz'],
            'foo:bar:baz',
        ];
        yield 'pipe seperated' => [
            ['foo', 'bar', 'baz'],
            'foo|bar|baz',
        ];
    }
}
