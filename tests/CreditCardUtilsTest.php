<?php

namespace Tests;

use Compass\Utils\CreditCardUtils;

class CreditCardUtilsTest extends UtilityTestCase
{

    public function testMaskCardNumber()
    {
        $this->assertSame('************8965', CreditCardUtils::maskCardNumber(5486231584568965));
        $this->assertSame('************8965', CreditCardUtils::maskCardNumber('5486231584568965'));
    }

    public function testMaskCardSecurityCode()
    {
        $this->assertSame('***', CreditCardUtils::maskCardSecurityCode(965));
        $this->assertSame('***', CreditCardUtils::maskCardSecurityCode('965'));
    }
}
