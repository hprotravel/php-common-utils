<?php

namespace Tests;

use Compass\Utils\Exception\FileNotFoundException;
use Compass\Utils\Exception\FileNotReadException;
use Compass\Utils\FileUtils;

class FileUtilsTest extends UtilityTestCase
{
    public function testExists()
    {
        $this->assertTrue(FileUtils::exists(path: 'tests/Fixtures/Resources/exists.txt'));
    }

    public function testRead()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = <<<JSON
{
    "foo": "bar"
}
JSON;

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::read(path: $path));
    }

    public function testReadThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to read contents.');

        FileUtils::read(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testJson()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = ['foo' => 'bar'];

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::json(path: $path));
    }

    public function testWrite()
    {
        $path = 'tests/Fixtures/Resources/write.json';

        \unlink($path); // remove if exists

        $content = <<<JSON
{
    "foo": "bar"
}
JSON;

        $this->assertTrue(FileUtils::write(path: $path, content: $content));
        $this->assertFileExists($path);
        $this->assertEquals($content, \file_get_contents($path));
    }

    public function testDelete()
    {
        $path = 'tests/Fixtures/Resources/delete.txt';

        $this->assertTrue(FileUtils::delete(path: $path));
        $this->assertFileDoesNotExist($path);

        \file_put_contents($path, 'delete me'); // add back for other tests
    }

    public function testCopy()
    {
        $source = 'tests/Fixtures/Resources/copy.txt';
        $destination = 'temp/copy.txt';

        $this->assertTrue(FileUtils::copy(source: $source, destination: $destination));
        $this->assertFileExists($destination);
        $this->assertEquals(\file_get_contents($source), \file_get_contents($destination));

        \unlink($destination); // remove after test
    }

    public function testMove()
    {
        $source = 'tests/Fixtures/Resources/move.txt';
        $destination = 'temp/move.txt';

        $this->assertTrue(FileUtils::move(source: $source, destination: $destination));
        $this->assertFileExists($destination);
        $this->assertFileDoesNotExist($source);

        \rename($destination, $source);
    }

    public function testGetMimeType()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'application/json';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getMimeType(path: $path));
    }

    public function testGetMimeTypeThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get mime type.');

        FileUtils::getMimeType(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testExtension()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'json';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getExtension(path: $path));
    }

    public function testExtensionThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get extension.');

        FileUtils::getExtension(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetFileName()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'read';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getFileName(path: $path));
    }

    public function testGetFileNameThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get file name.');

        FileUtils::getFileName(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetDirName()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'tests/Fixtures/Resources';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getDirName(path: $path));
    }

    public function testGetDirNameThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get directory name.');

        FileUtils::getDirName(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetBaseName()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'read.json';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getBaseName(path: $path));
    }

    public function testGetBaseNameThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get base name.');

        FileUtils::getBaseName(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetRealPath()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = __DIR__.'/Fixtures/Resources/read.json';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getRealPath(path: $path));
    }

    public function testGetRealPathThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get real path.');

        FileUtils::getRealPath(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetFileSize()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 20;

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getFileSize(path: $path));
    }

    public function testGetFileSizeThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get file size.');

        FileUtils::getFileSize(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetFileOwner()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 502;

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getFileOwner(path: $path));
    }

    public function testGetFileOwnerThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get file owner.');

        FileUtils::getFileOwner(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetHash()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 'c4739e97d2e782ab9142b7cad4e36383';

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getHash(path: $path));
    }

    public function testGetHashThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get hash.');

        FileUtils::getHash(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testGetPermissions()
    {
        $path = 'tests/Fixtures/Resources/read.json';
        $expected = 33188;

        $this->assertFileExists($path);
        $this->assertEquals($expected, FileUtils::getPermissions(path: $path));
    }

    public function testGetPermissionsThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Cannot access "tests/Fixtures/Resources/not-exists.json" to get permissions.');

        FileUtils::getPermissions(path: 'tests/Fixtures/Resources/not-exists.json');
    }

    public function testChmod()
    {
        $path = 'tests/Fixtures/Resources/chmod.txt';

        $this->assertFileExists($path);
        $this->assertTrue(FileUtils::chmod(path: $path, mode: 0755));
        $this->assertEquals(33261, FileUtils::getPermissions(path: $path));
        $this->assertTrue(FileUtils::chmod(path: $path, mode: 0644));
        $this->assertEquals(33261, FileUtils::getPermissions(path: $path));
    }
}