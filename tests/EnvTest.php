<?php

namespace Tests;

use Compass\Utils\Env;
use Compass\Utils\Environment;
use PHPUnit\Framework\TestCase;

class EnvTest extends TestCase
{
    protected function setUp(): void
    {
        $_ENV[Env::ENV_KEY] = Environment::TEST->value;
    }

    public function testGet()
    {
        $this->assertEquals(Environment::TEST->get(), Env::get());
        $this->assertNotEquals(Environment::DEV->get(), Env::get());
    }

    public function testIs()
    {
        $this->assertTrue(Env::isTesting());
        $this->assertFalse(Env::isDevelopment());
        $this->assertFalse(Env::isStaging());
        $this->assertFalse(Env::isProduction());
    }
}