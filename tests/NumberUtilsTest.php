<?php

namespace Tests;

use Compass\Utils\NumberUtils;

class NumberUtilsTest extends UtilityTestCase
{

    public function testFormat()
    {
        $this->assertSame('99.99', NumberUtils::format(99.99));
        $this->assertEquals('99.99', NumberUtils::format(99.99));
        $this->assertEquals('99.99', NumberUtils::format(99.991));
        $this->assertEquals('100.00', NumberUtils::format(99.999));
    }

    /**
     * @dataProvider provideOrdinalData
     */
    public function testOrdinal(string $expected, int $number)
    {
        $this->assertEquals($expected, NumberUtils::ordinal($number));
    }

    public function provideOrdinalData(): \Generator
    {
        yield ['0th', 0];
        yield ['1st', 1];
        yield ['2nd', 2];
        yield ['3rd', 3];
        yield ['4th', 4];
        yield ['5th', 5];
        yield ['6th', 6];
        yield ['7th', 7];
        yield ['8th', 8];
        yield ['9th', 9];
        yield ['10th', 10];
        yield ['11th', 11];
    }

    public function testBetween()
    {
        $this->assertCount(10, NumberUtils::between(0, 9));
        $this->assertCount(2, NumberUtils::between(0, 9, 2));
        $this->assertCount(10, NumberUtils::between(0, 9));
        $this->assertCount(10, NumberUtils::between(9, 0));
        $this->assertCount(10, NumberUtils::between(0, 900, 10, 100));
        $this->assertCount(5, NumberUtils::between(0, 900, 5, 100));
    }

    public function testFindPercent()
    {
        $this->assertEquals(10, NumberUtils::findPercent(100, 110));
        $this->assertEquals(-10, NumberUtils::findPercent(100, 90));
        $this->assertEquals(10.989999999999995, NumberUtils::findPercent(100, 110.99));
        $this->assertEquals(-19.010000000000005, NumberUtils::findPercent(100, 80.99));
    }

    public function testApplyPercent()
    {
        $result = 0;

        $this->assertEquals(110, NumberUtils::applyPercent(100, 10, $result));
        $this->assertEquals(10, $result);
        $this->assertEquals(90, NumberUtils::applyPercent(100, -10, $result));
        $this->assertEquals(-10, $result);
        $this->assertEquals(110.99, NumberUtils::applyPercent(100, 10.99, $result));
        $this->assertEquals(10.99, $result);
        $this->assertEquals(90.01, NumberUtils::applyPercent(100, -9.99, $result));
        $this->assertEquals(-9.99, $result);
    }

    public function testRoundPercent()
    {
        $this->assertEquals(109.99, NumberUtils::roundPercent(100, 9.991));
    }

}