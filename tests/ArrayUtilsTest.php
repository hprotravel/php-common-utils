<?php

namespace Tests;

use Compass\Utils\ArrayUtils;

class ArrayUtilsTest extends UtilityTestCase
{

    public function testExists()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertTrue(ArrayUtils::exists($array, 'foo'));
        $this->assertFalse(ArrayUtils::exists($array, 'invalid'));
    }

    public function testExcept()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(['bar' => 'baz'], ArrayUtils::except($array, 'foo'));
        $this->assertSame(['foo' => true, 'bar' => 'baz'], ArrayUtils::except($array, 'invalid'));
    }

    public function testFirst()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(null, ArrayUtils::first([]));
        $this->assertSame(true, ArrayUtils::first($array));
        $this->assertSame(
            'baz',
            ArrayUtils::first($array, function ($value, $key) {
                return 'baz' === $value;
            })
        );
        $this->assertSame(
            'baz',
            ArrayUtils::first($array, function ($value, $key) {
                return 'bar' === $key;
            })
        );
    }

    public function testLast()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(null, ArrayUtils::last([]));
        $this->assertSame('baz', ArrayUtils::last($array));
        $this->assertSame(
            'baz',
            ArrayUtils::last($array, function ($value, $key) {
                return 'baz' === $value;
            })
        );
        $this->assertSame(
            true,
            ArrayUtils::last($array, function ($value, $key) {
                return 'foo' === $key;
            })
        );
    }

    public function testGet()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(null, ArrayUtils::get([$array], 'invalid'));
        $this->assertSame(true, ArrayUtils::get($array, 'foo'));
    }

    public function testForget()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        ArrayUtils::forget($array, 'foo');

        $this->assertSame(['bar' => 'baz'], $array);
    }

    public function testHas()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertTrue(ArrayUtils::has($array, 'foo'));
        $this->assertTrue(ArrayUtils::has($array, ['foo', 'bar']));
        $this->assertFalse(ArrayUtils::has($array, 'invalid'));
    }

    public function testGroup()
    {
        $array = [
            ['foo' => true, 'bar' => 'baz'],
            ['foo' => false, 'bar' => 'baz'],
            ['foo' => true, 'bar' => 'buz'],
        ];

        $this->assertSame(
            [
                'baz' => [['foo' => true, 'bar' => 'baz'], ['foo' => false, 'bar' => 'baz']],
                'buz' => [['foo' => true, 'bar' => 'buz']],
            ],
            ArrayUtils::group($array, 'bar')
        );
    }

    public function testAssoc()
    {
        $array = ['foo' => true, 'bar' => 'baz'];
        $nonAssocArray = [true, 'baz'];

        $this->assertTrue(ArrayUtils::assoc($array));
        $this->assertFalse(ArrayUtils::assoc($nonAssocArray));
    }

    /**
     * @dataProvider provideConsecData
     */
    public function testConsec(bool $expected, array $input)
    {
        $this->assertEquals($expected, ArrayUtils::consec($input));
    }

    public function provideConsecData(): \Generator
    {
        yield [true, [1, 2, 3, 4, 5, 6, 7]];
        yield [true, ['a', 'b', 'c', 'd', 'e', 'f']];
        yield [true, [3, 4, 5, 6, 7]];
        yield [true, ['c', 'd', 'e', 'f']];
        yield [false, [7, 3, 4, 5, 6]];
        yield [false, ['f', 'c', 'd', 'e']];
    }

    public function testOnly()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(['foo' => true], ArrayUtils::only($array, 'foo'));
        $this->assertSame(['foo' => true, 'bar' => 'baz'], ArrayUtils::only($array, ['foo', 'bar']));
    }

    public function testDot()
    {
        $array = ['foo' => true, 'bar' => ['baz' => 'baz']];

        $this->assertSame(['foo' => true, 'bar.baz' => 'baz'], ArrayUtils::dot($array));
    }

    public function testUndot()
    {
        $array = ['foo' => true, 'bar.baz' => 'baz'];

        $this->assertSame(['foo' => true, 'bar' => ['baz' => 'baz']], ArrayUtils::undot($array));
    }

    public function testPrepend()
    {
        $array = ['bar' => 'baz'];

        $this->assertSame(['foo' => true, 'bar' => 'baz'], ArrayUtils::prepend($array, true, 'foo'));
    }

    public function testPull()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $value = ArrayUtils::pull($array, 'foo');

        $this->assertSame(true, $value);
        $this->assertSame(['bar' => 'baz'], $array);
    }

    public function testRandom()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $value = ArrayUtils::random($array);

        $this->assertFalse(is_array($value));

        $value = ArrayUtils::random($array, 2);

        $this->assertSame([true, 'baz'], $value);

        $value = ArrayUtils::random($array, 2, true);

        $this->assertSame($array, $value);
    }

    public function testSet()
    {
        $array = ['foo' => true];

        ArrayUtils::set($array, 'bar.baz', 'baz');

        $this->assertSame(['foo' => true, 'bar' => ['baz' => 'baz']], $array);
    }

    public function testShuffle()
    {
        $array = [1, 2, 3, 4, 5, 6, 7];

        $this->assertNotSame($array, ArrayUtils::shuffle($array));
    }

    public function testFlatten()
    {
        $array = ['foo' => ['bar' => ['baz' => 'buzz']]];

        $this->assertSame(['buzz'], ArrayUtils::flatten($array));
    }

    public function testAdd()
    {
        $array = ['foo' => true];

        $this->assertSame(['foo' => true, 'bar' => false], ArrayUtils::add($array, 'bar', false));
    }

    public function testSortBy()
    {
        $array = [
            ['foo' => ['bar' => ['baz' => 'buzz']], 'id' => 3],
            ['bar' => ['bar' => ['baz' => 'buzz']], 'id' => 2],
            ['baz' => ['bar' => ['baz' => 'buzz']], 'id' => 1],
        ];

        self::assertSame(
            [
                ['baz' => ['bar' => ['baz' => 'buzz']], 'id' => 1],
                ['bar' => ['bar' => ['baz' => 'buzz']], 'id' => 2],
                ['foo' => ['bar' => ['baz' => 'buzz']], 'id' => 3],
            ],
            ArrayUtils::sortBy($array, 'id')
        );
    }

    public function testList(): void
    {
        $this->assertIsArray(ArrayUtils::list(1, 10));
        $this->assertEquals([1 => 1, 2 => 2, 3 => 3], ArrayUtils::list(1, 3));
    }

    public function testReplace()
    {
        $array1 = ['foo' => true];
        $array2 = ['bar' => 'baz'];

        $this->assertSame(['foo' => true, 'bar' => 'baz'], ArrayUtils::replace($array1, $array2));
    }

    public function testDepth()
    {
        $array = ['foo' => ['bar' => ['baz' => 'buzz']]];

        $this->assertEquals(3, ArrayUtils::depth($array));
    }

    public function testQuery()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertEquals('foo=1&bar=baz', ArrayUtils::query($array));
    }

    public function testWhere()
    {
        $array = ['foo' => true, 'bar' => 'baz'];

        $this->assertSame(['foo' => true], ArrayUtils::where($array, fn($value) => $value === true));
        $this->assertSame(['foo' => true], ArrayUtils::where($array, fn($value, $key) => $key === 'foo'));
        $this->assertSame(['bar' => 'baz'], ArrayUtils::where($array, fn($value, $key) => $key === 'bar'));
    }

    /**
     * @dataProvider provideSearchData
     */
    public function testSearch(array $expected, array $input, string $key, mixed $value)
    {
        $this->assertEquals($expected, ArrayUtils::search($input, $key, $value));
    }

    public function provideSearchData(): \Generator
    {
        yield [['foo' => true, 'bar' => 'baz'], ['foo' => true, 'bar' => 'baz'], 'bar', 'baz'];
        yield [
            ['foo' => true, 'bar' => ['baz' => 'buzz']],
            ['foo' => true, 'bar' => ['baz' => 'buzz']],
            'bar',
            ['baz' => 'buzz'],
        ];
        yield [['baz' => 'buzz'], ['foo' => true, 'bar' => ['baz' => 'buzz']], 'baz', 'buzz'];
        yield [['baz' => 'buzz'], ['foo' => true, ['baz' => 'buzz'], 'bar' => ['baz' => 'buzz']], 'baz', 'buzz'];
        yield [
            ['baz' => 'buzz', 'bar' => 'baz'],
            ['foo' => true, 'bar' => ['baz' => 'buzz', 'bar' => 'baz'], ['baz' => 'buzz']],
            'baz',
            'buzz',
        ];
    }

    /**
     * @dataProvider provideClosestData
     */
    public function testClosest(
        int $expected,
        array $input,
        int $value,
        string $method = ArrayUtils::CLOSEST_METHOD_CLOSEST
    ) {
        $this->assertEquals($expected, ArrayUtils::closest($input, $value, $method));
    }

    public function provideClosestData(): \Generator
    {
        yield [2, [1, 2, 3, 4, 5, 6, 7], 2];
        yield [1, [1, 3, 4, 5, 6, 7], 1];
        yield [5, [1, 5, 6, 7], 4];
        yield [5, [1, 5, 6, 7], 3];
        yield [1, [1, 5, 6, 7], 2, ArrayUtils::CLOSEST_METHOD_CLOSEST];
        yield [5, [1, 5, 6, 7], 2, ArrayUtils::CLOSEST_METHOD_HIGHER];
        yield [1, [1, 5, 6, 7], 3, ArrayUtils::CLOSEST_METHOD_LOWER];
    }

    public function testRemoveEmpty()
    {
        $array = ['foo' => true, 'bar' => []];

        $this->assertSame(['foo' => true], ArrayUtils::removeEmpty($array));
    }

    public function testInArrayRecursive()
    {
        $array = ['foo' => true, 'bar' => ['baz' => 'qux']];

        $this->assertTrue(ArrayUtils::inArrayRecursive('qux', $array));
        $this->assertTrue(ArrayUtils::inArrayRecursive('baz', $array));
        $this->assertFalse(ArrayUtils::inArrayRecursive('baz', $array, true));
    }

    public function testKsortRecursive()
    {
        $array = [1 => '1', 2 => '2', 6 => [1 => '6'], 3 => [2 => '3', 1 => '4'], 5 => '5'];

        ArrayUtils::ksortRecursive($array);

        $this->assertSame([1 => '1', 2 => '2', 3 => [1 => '4', 2 => '3'], 5 => '5', 6 => [1 => '6']], $array);
    }

    public function testEmptyRecursive()
    {
        $array = ['foo' => true, 'bar' => ['buz' => []]];

        $this->assertFalse(ArrayUtils::emptyRecursive($array));

        $array = ['foo' => ['bar' => ['buz' => []]]];

        $this->assertTrue(ArrayUtils::emptyRecursive($array));
    }

    public function testDiffAssocRecursive()
    {
        $source = ['foo' => true, 'bar' => ['baz' => ['qux' => 'fred']]];
        $target = ['foo' => true, 'bar' => ['baz' => ['thud' => 'fred']]];

        $this->assertEquals(['bar' => ['baz' => ['qux' => 'fred']]], ArrayUtils::diffAssocRecursive($source, $target));
    }

    public function testMapWithKeys()
    {
        $array = [
            'foo' => 'Foo',
            'bar' => 'bar',
            'Baz' => 'Baz',
        ];
        $expected = [
            'foo' => 'Foo',
            'bar' => 'Bar',
            'Baz' => 'Baz',
        ];

        $actual = ArrayUtils::mapWithKeys(
            $array,
            function (&$key, $value) {
                if ('bar' == $key) {
                    $value = ucfirst($value);
                }

                if ('Baz' == $key) {
                    $key = lcfirst($key);
                }

                return $value;
            }
        );

        $this->assertSame($expected, $actual);
    }

    public function testMapWithKeysWithoutAnyChanges()
    {
        $array = [
            'foo' => 'Foo',
            'bar' => 'Bar',
            'baz' => 'Baz',
        ];
        $expected = [
            'foo' => 'Foo',
            'bar' => 'Bar',
            'baz' => 'Baz',
        ];

        $actual = ArrayUtils::mapWithKeys(
            $array,
            function ($key, $value) {
                if ('buzz' == $key) {
                    $value = ucfirst($value);
                }

                return $value;
            }
        );

        $this->assertSame($expected, $actual);
    }

    public function testImplodeWithKeys()
    {
        $this->assertEquals(
            'foo=\'bar\', baz=\'qux\'',
            ArrayUtils::implodeWithKeys(', ', ['foo' => 'bar', 'baz' => 'qux'])
        );
    }

    public function testWrap()
    {
        $var = 'foo';

        $this->assertSame(['foo'], ArrayUtils::wrap($var));
    }

    public function testAccessible()
    {
        $array = ['foo', 'bar', 'baz'];

        $this->assertTrue(ArrayUtils::accessible($array));
    }

    public function testPermutation()
    {
        $given = [['A'], ['B'], ['C']];

        $expected = [['A', 'B', 'C']];

        $this->assertSame($expected, ArrayUtils::permutation($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B', 'C'],
        ];

        $expected = [
            ['A', 'A',],
            ['A', 'B',],
            ['A', 'C',],
            ['B', 'A',],
            ['B', 'B',],
            ['B', 'C',],
            ['C', 'A',],
            ['C', 'B',],
            ['C', 'C',],
        ];

        $this->assertSame($expected, ArrayUtils::permutation($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B'],
        ];

        $this->assertSame([
            ['A', 'A',],
            ['A', 'B',],
            ['B', 'A',],
            ['B', 'B',],
            ['C', 'A',],
            ['C', 'B',],
        ], ArrayUtils::permutation($given));

        $given = [
            ['A', 'B'],
            ['C', 'D'],
            ['E', 'F'],
        ];

        $expected = [
            ['A', 'C', 'E'],
            ['A', 'C', 'F'],
            ['A', 'D', 'E'],
            ['A', 'D', 'F'],
            ['B', 'C', 'E'],
            ['B', 'C', 'F'],
            ['B', 'D', 'E'],
            ['B', 'D', 'F'],
        ];

        $this->assertSame($expected, ArrayUtils::permutation($given));

        $given = [
            ['1', '2', '3'],
            ['4', '5'],
            ['6'],
        ];

        $expected = [
            ['1', '4', '6'],
            ['1', '5', '6'],
            ['2', '4', '6'],
            ['2', '5', '6'],
            ['3', '4', '6'],
            ['3', '5', '6'],
        ];

        $this->assertSame($expected, ArrayUtils::permutation($given));

        $given = [
            ['X', 'Y'],
            ['Z'],
        ];

        $expected = [
            ['X', 'Z'],
            ['Y', 'Z'],
        ];

        $this->assertSame($expected, ArrayUtils::permutation($given));
    }

    public function testPermute()
    {
        $given = [['A'], ['B'], ['C']];

        $expected = [['A', 'B', 'C']];

        $this->assertSame($expected, ArrayUtils::permute($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B', 'C'],
        ];

        $expected = [
            ['A', 'A',],
            ['A', 'B',],
            ['A', 'C',],
            ['B', 'A',],
            ['B', 'B',],
            ['B', 'C',],
            ['C', 'A',],
            ['C', 'B',],
            ['C', 'C',],
        ];

        $this->assertSame($expected, ArrayUtils::permute($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B'],
        ];

        $this->assertSame([
            ['A', 'A',],
            ['A', 'B',],
            ['B', 'A',],
            ['B', 'B',],
            ['C', 'A',],
            ['C', 'B',],
        ], ArrayUtils::permute($given));

        $given = [
            ['A', 'B'],
            ['C', 'D'],
            ['E', 'F'],
        ];

        $expected = [
            ['A', 'C', 'E'],
            ['A', 'C', 'F'],
            ['A', 'D', 'E'],
            ['A', 'D', 'F'],
            ['B', 'C', 'E'],
            ['B', 'C', 'F'],
            ['B', 'D', 'E'],
            ['B', 'D', 'F'],
        ];

        $this->assertSame($expected, ArrayUtils::permute($given));

        $given = [
            ['1', '2', '3'],
            ['4', '5'],
            ['6'],
        ];

        $expected = [
            ['1', '4', '6'],
            ['1', '5', '6'],
            ['2', '4', '6'],
            ['2', '5', '6'],
            ['3', '4', '6'],
            ['3', '5', '6'],
        ];

        $this->assertSame($expected, ArrayUtils::permute($given));

        $given = [
            ['X', 'Y'],
            ['Z'],
        ];

        $expected = [
            ['X', 'Z'],
            ['Y', 'Z'],
        ];

        $this->assertSame($expected, ArrayUtils::permute($given));
    }

    public function testCombine()
    {
        $given = [
            ['A'],
            ['B'],
            ['C'],
        ];

        $expected = [
            ['A', 'B', 'C'],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B', 'C'],
        ];

        $expected = [
            ['A', 'A',],
            ['A', 'B',],
            ['A', 'C',],
            ['B', 'B',],
            ['B', 'C',],
            ['C', 'C',],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));

        $given = [
            ['A', 'B', 'C'],
            ['A', 'B'],
        ];

        $expected = [
            ['A', 'A',],
            ['A', 'B',],
            ['B', 'B',],
            ['A', 'C',],
            ['B', 'C',],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));

        $given = [
            ['A', 'B'],
            ['C', 'D'],
            ['E', 'F'],
        ];

        $expected = [
            ['A', 'C', 'E'],
            ['A', 'C', 'F'],
            ['A', 'D', 'E'],
            ['A', 'D', 'F'],
            ['B', 'C', 'E'],
            ['B', 'C', 'F'],
            ['B', 'D', 'E'],
            ['B', 'D', 'F'],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));

        $given = [
            ['1', '2', '3'],
            ['4', '5'],
            ['6'],
        ];

        $expected = [
            ['1', '4', '6'],
            ['1', '5', '6'],
            ['2', '4', '6'],
            ['2', '5', '6'],
            ['3', '4', '6'],
            ['3', '5', '6'],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));

        $given = [
            ['X', 'Y'],
            ['Z'],
        ];

        $expected = [
            ['X', 'Z'],
            ['Y', 'Z'],
        ];

        $this->assertSame($expected, ArrayUtils::combine($given));
    }

    /**
     * @dataProvider provideJoinData
     */
    public function testJoin(string $expected, array $input, string $separator = ',')
    {
        $this->assertEquals($expected, ArrayUtils::join($input, $separator));
    }

    public static function provideJoinData(): \Generator
    {
        yield 'comma separated' => [
            'foo,bar,baz',
            ['foo', 'bar', 'baz'],
        ];
        yield 'colon separated' => [
            'foo:bar:baz',
            ['foo', 'bar', 'baz'],
            ':',
        ];
        yield 'pipe separated' => [
            'foo|bar|baz',
            ['foo', 'bar', 'baz'],
            '|',
        ];
    }

    // TODO: add more tests...
}