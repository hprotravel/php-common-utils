<?php

namespace Tests;

use Compass\Utils\HtmlUtils;

class HtmlUtilsTest extends UtilityTestCase
{
    private static string $html = <<<HTML
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
  </body>
</html>
HTML;

    public function testValid()
    {
        $this->assertTrue(HtmlUtils::valid(self::$html));
    }

    public function testCompress()
    {
        $expected = <<<HTML
<!doctype html><html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <title>Bootstrap demo</title> </head> <body> <h1>Hello, world!</h1> </body></html>
HTML;
        $this->assertEquals($expected, HtmlUtils::compress(self::$html));
    }

}