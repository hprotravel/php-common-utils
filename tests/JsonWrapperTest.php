<?php

namespace Tests;

use Compass\Utils\JsonWrapper;

class JsonWrapperTest extends UtilityTestCase
{
    public function testJsonDecode()
    {
        $json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';

        $actual = JsonWrapper::decode($json, true);

        $expected = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];

        $this->assertEquals($expected, $actual);
    }

    public function testJsonDecodeInvalidJson()
    {
        $this->expectExceptionMessage("Syntax error");
        $this->expectException(\InvalidArgumentException::class);
        $json = '{"a":1,"b":2,"c":3,"d":4,"e":5,}';

        JsonWrapper::decode($json);
    }

    public function testJsonDecodeEmptyJson()
    {
        $json = '';

        $actual = JsonWrapper::decode($json);

        $expected = [];

        $this->assertEquals($expected, $actual);
    }

    public function testJsonDecodeEmptyJsonAsObject()
    {
        $json = '';

        $actual = JsonWrapper::decode($json, false);

        $expected = null;

        $this->assertEquals($expected, $actual);
    }

    public function testJsonDecodeArrayJson()
    {
        $this->expectExceptionMessage("Invalid JSON data.");
        $this->expectException(\InvalidArgumentException::class);
        $json = ["a" => 1, "b" => 2, "c" => 3, "d" => 4, "e" => 5];

        JsonWrapper::decode($json);
    }

    public function testJsonDecodeArrayStringJson()
    {
        $this->expectExceptionMessage("Syntax error");
        $this->expectException(\InvalidArgumentException::class);
        $json = '["a" => 1, "b" => 2, "c" => 3, "d" => 4, "e" => 5]';

        JsonWrapper::decode($json);
    }

    public function testPretty()
    {
        $json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';

        $expected = <<<JSON
{
    "a": 1,
    "b": 2,
    "c": 3,
    "d": 4,
    "e": 5
}
JSON;

        $this->assertEquals($expected, JsonWrapper::pretty($json));
    }
}