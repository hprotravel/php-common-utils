<?php

namespace Tests;

use Compass\Utils\CurrencyUtils;

class CurrencyUtilsTest extends UtilityTestCase
{
    private static array $currencies = [];

    protected function setUp(): void
    {
        self::loadCurrencies();
    }

    private static function loadCurrencies(): void
    {
        if ([] === self::$currencies) {
            self::$currencies = require __DIR__ . '/../resources/currencies.php';
        }
    }

    public function testCurrencies(): void
    {
        $this->assertEquals(self::$currencies, CurrencyUtils::currencies());
    }

    public function testCurrencyCodes(): void
    {
        $this->assertEquals(\array_column(self::$currencies, 'code'), CurrencyUtils::currencyCodes());
    }

    public function testCurrencyNames(): void
    {
        $this->assertEquals(\array_column(self::$currencies, 'name'), CurrencyUtils::currencyNames());
    }

    public function testCurrencyExists(): void
    {
        $this->assertTrue(CurrencyUtils::currencyExists('USD'));
        $this->assertFalse(CurrencyUtils::currencyExists('SSS'));
    }

    /**
     * @dataProvider provideCurrencyData
     */
    public function testCurrency(array $expected, string $code): void
    {
        $this->assertEquals($expected, CurrencyUtils::currency($code));
    }

    private static function provideCurrencyData(): \Generator
    {
        yield [
            [
                'code' => 'TRY',
                'name' => 'Turkish Lira',
                'symbol' => '₺',
            ],
            'TRY',
        ];
        yield [
            [
                'code' => 'USD',
                'name' => 'US Dollar',
                'symbol' => '$',
            ],
            'USD',
        ];
    }

    /**
     * @dataProvider provideCurrencyNameData
     */
    public function testCurrencyName(string $expected, string $code): void
    {
        $this->assertEquals($expected, CurrencyUtils::currencyName($code));
    }

    private static function provideCurrencyNameData(): \Generator
    {
        yield ['US Dollar', 'USD'];
        yield ['Euro', 'EUR'];
        yield ['Turkish Lira', 'TRY'];
    }

    /**
     * @dataProvider provideCurrencySymbolData
     */
    public function testCurrencySymbol(string $expected, string $code): void
    {
        $this->assertEquals($expected, CurrencyUtils::currencySymbol($code));
    }

    private static function provideCurrencySymbolData(): \Generator
    {
        yield ['$', 'USD'];
        yield ['€', 'EUR'];
        yield ['£', 'GBP'];
        yield ['¥', 'JPY'];
        yield ['₣', 'CHF'];
        yield ['CN¥', 'CNY'];
        yield ['₹', 'INR'];
        yield ['R$', 'BRL'];
        yield ['₽', 'RUB'];
        yield ['MX$', 'MXN'];
        yield ['HK$', 'HKD'];
        yield ['₩', 'KRW'];
        yield ['₺', 'TRY'];
        yield ['₫', 'VND'];
        yield ['₪', 'ILS'];
    }

    /**
     * @dataProvider provideCurrencyHasSymbolData
     */
    public function testCurrencyHasSymbol(string $code): void
    {
        $this->assertTrue(CurrencyUtils::currencyHasSymbol($code));
    }

    private static function provideCurrencyHasSymbolData(): \Generator
    {
        yield ['USD'];
        yield ['TRY'];
    }

    /**
     * @dataProvider provideCurrencyHasNotSymbolData
     */
    public function testCurrencyHasNotSymbol(string $code): void
    {
        $this->assertFalse(CurrencyUtils::currencyHasSymbol($code));
    }

    private static function provideCurrencyHasNotSymbolData(): \Generator
    {
        yield ['KWD'];
        yield ['SAR'];
    }


    /**
     * @dataProvider provideRoundData
     */
    public function testRound(float|int $expected, float|int $amount): void
    {
        $this->assertSame($expected, CurrencyUtils::round($amount));
    }

    private static function provideRoundData(): \Generator
    {
        yield [9.00, 9];
        yield [9.99, 9.9888888];
        yield [9.95, 9.9499999999];
        yield [999777.77, 999777.77];
    }

    /**
     * @dataProvider provideFormatData
     */
    public function testFormat(string $expected, float|int $amount, string $code, string $locale = 'en'): void
    {
        $this->assertEquals($expected, \currency_format($amount, $code, $locale));
    }

    private static function provideFormatData(): \Generator
    {
        yield ['$9.00', 9, 'USD'];
        yield ['-$9.00', -9, 'USD'];
        yield ['$10.00', 9.9977777, 'USD'];
        yield ['$9.95', 9.9499999999, 'USD'];
        yield ['$999,777.77', 999777.77, 'USD'];
        yield ['$9.95', 9.9499999999, 'USD'];
        yield ['$1,234.56', 1234.56, 'USD'];
        yield ['$1.234,56', 1234.56, 'USD', 'tr'];
        yield ['1 234,56 $US', 1234.56, 'USD', 'fr'];
        yield ['TRY 1,234.56', 1234.56, 'TRY'];
        yield ['₺1.234,56', 1234.56, 'TRY', 'tr'];
        yield ['€1,234.56', 1234.56, 'EUR'];
        yield ['₩1,235', 1234.56, 'KRW'];
        yield ['KWD 1,234.560', 1234.56, 'KWD'];
        yield ['SAR 1,234.56', 1234.56, 'SAR'];
        yield ['AED 1,234.56', 1234.56, 'AED'];
        yield ['AMD 1,234.56', 1234.56, 'AMD'];
        yield ['IDR 1,234.56', 1234.56, 'IDR'];
        yield ['₪1,234.56', 1234.56, 'ILS'];
        yield ['₹1,234.56', 1234.56, 'INR'];
        yield ['£1,234.56', 1234.56, 'GBP'];
        yield ['¥1,235', 1234.56, 'JPY'];
        yield ['CHF 1,234.56', 1234.56, 'CHF'];
    }
}