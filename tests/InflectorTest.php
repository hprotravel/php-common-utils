<?php

namespace Tests;

use Compass\Utils\Inflector;
use PHPUnit\Framework\TestCase;

class InflectorTest extends TestCase
{
    /**
     * @dataProvider providePluralizeData
     * @param string $expected
     * @param $word
     */
    public function testPluralize(string $expected, $word)
    {
        $this->assertEquals($expected, Inflector::pluralize($word));
    }

    public function providePluralizeData(): \Generator
    {
        yield ['people', 'person'];
        yield ['statuses', 'status'];
        yield ['babies', 'baby'];
        yield ['cars', 'car'];
        yield ['children', 'child'];
        yield ['homes', 'home'];
    }

    /**
     * @dataProvider provideSingularizeData
     * @param string $expected
     * @param $word
     */
    public function testSingularize(string $expected, $word)
    {
        $this->assertEquals($expected, Inflector::singularize($word));
    }

    public function provideSingularizeData(): \Generator
    {
        yield ['person', 'people'];
        yield ['status', 'statuses'];
        yield ['baby', 'babies'];
        yield ['car', 'cars'];
        yield ['child', 'children'];
        yield ['home', 'homes'];
    }
}
