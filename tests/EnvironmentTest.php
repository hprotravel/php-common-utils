<?php

namespace Tests;

use Compass\Utils\Environment;
use PHPUnit\Framework\TestCase;

class EnvironmentTest extends TestCase
{

    public function testGet()
    {
        $environment = Environment::TEST;

        $this->assertEquals('test', $environment->get());
    }

    public function testIs()
    {
        $environment = Environment::TEST;

        $this->assertTrue($environment->is(Environment::TEST));
        $this->assertFalse($environment->is(Environment::DEV));
    }
}
