<?php

namespace Tests;

use Compass\Utils\CommonUtils;
use Tests\Fixtures\Exception\RetryException;

class CommonUtilsTest extends UtilityTestCase
{

    public function testRetry()
    {
        $attempts = 0;

        CommonUtils::retry(3, function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new \Exception();
            }
        }, 100);

        $this->assertEquals(3, $attempts);
    }

    public function testRetryWithCustomTimes()
    {
        $attempts = 0;

        CommonUtils::retry(3, function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new \Exception();
            }
        }, function (int $attempt, \Exception $e) {
            return $attempt * 100;
        });

        $this->assertEquals(3, $attempts);
    }

    public function testRetryWithBackoffTimes()
    {
        $attempts = 0;

        CommonUtils::retry([300, 200, 100], function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new \Exception();
            }
        });

        $this->assertEquals(3, $attempts);
    }

    public function testRetryWithSpecificException()
    {
        $this->expectException(\Exception::class);

        $attempts = 0;

        \retry(3, function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new \Exception();
            }
        }, 100, function (\Exception $e) {
            return $e instanceof RetryException;
        });

        $this->assertEquals(3, $attempts);
    }

    public function testRetryWithSpecificExceptionNotThrown()
    {
        $attempts = 0;

        \retry(3, function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new RetryException();
            }
        }, 100, function (\Exception $e) {
            return $e instanceof RetryException;
        });

        $this->assertEquals(3, $attempts);
    }

    public function testRetryWithRandomBackoffTimes()
    {
        $attempts = 0;

        CommonUtils::retry(\array_map(fn($times) => $times * 100, [1, 2, 3]), function () use (&$attempts) {
            $attempts += 1;

            if ($attempts < 3) {
                throw new \Exception();
            }
        });

        $this->assertEquals(3, $attempts);
    }
}
