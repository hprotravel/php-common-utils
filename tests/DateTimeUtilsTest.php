<?php

namespace Tests;

use Compass\Utils\DateTimeUtils;

class DateTimeUtilsTest extends UtilityTestCase
{
    const DATETIME_STRING = '2022-02-02 02:02:02';
    const DATE_STRING = '2022-02-02';

    public function testDate()
    {
        $this->assertInstanceOf(\DateTime::class, DateTimeUtils::date());
        $this->assertInstanceOf(\DateTimeInterface::class, DateTimeUtils::now());

        $this->assertEquals(
            self::DATETIME_STRING,
            DateTimeUtils::date(self::DATETIME_STRING)->format(DateTimeUtils::DATETIME_FORMAT),
        );
        $this->assertEquals(
            (new \DateTime())->format(DateTimeUtils::DATETIME_FORMAT),
            DateTimeUtils::now()->format(DateTimeUtils::DATETIME_FORMAT),
        );
    }

    public function testAddInterval()
    {
        $this->assertEquals('2022-02-02', DateTimeUtils::addInterval('2022-02-02', 'P0D'));
        $this->assertEquals('2022-02-03', DateTimeUtils::addInterval('2022-02-02', 'P1D'));
        $this->assertEquals('2022-03-02', DateTimeUtils::addInterval('2022-02-02', 'P1M'));
        $this->assertEquals('2023-02-02', DateTimeUtils::addInterval('2022-02-02', 'P1Y'));
        $this->assertEquals(
            '2023-02-02 22:22:22',
            DateTimeUtils::addInterval('2022-02-02 22:22:22', 'P1Y', DateTimeUtils::DATETIME_FORMAT),
        );
        $this->assertEquals(
            '2023-02-02 22:22:22',
            DateTimeUtils::addInterval(new \DateTime('2022-02-02 22:22:22'), 'P1Y', DateTimeUtils::DATETIME_FORMAT),
        );
    }

    public function testSubtractInterval()
    {
        $this->assertEquals('2022-02-02', DateTimeUtils::subtractInterval('2022-02-02', 'P0D'));
        $this->assertEquals('2022-02-01', DateTimeUtils::subtractInterval('2022-02-02', 'P1D'));
        $this->assertEquals('2022-01-02', DateTimeUtils::subtractInterval('2022-02-02', 'P1M'));
        $this->assertEquals('2021-02-02', DateTimeUtils::subtractInterval('2022-02-02', 'P1Y'));
        $this->assertEquals(
            '2021-02-02 22:22:22',
            DateTimeUtils::subtractInterval('2022-02-02 22:22:22', 'P1Y', DateTimeUtils::DATETIME_FORMAT),
        );
        $this->assertEquals(
            '2021-02-02 22:22:22',
            \date_sub_interval(new \DateTime('2022-02-02 22:22:22'), 'P1Y', DateTimeUtils::DATETIME_FORMAT),
        );
    }


    public function testDateString()
    {
        $this->assertEquals((new \DateTime('today'))->format(DateTimeUtils::DATE_FORMAT), DateTimeUtils::dateString());
        $this->assertEquals(self::DATE_STRING, DateTimeUtils::dateString(self::DATE_STRING));
        $this->assertEquals(self::DATE_STRING, DateTimeUtils::dateString(self::DATETIME_STRING));
    }

    public function testDateTimeString()
    {
        $this->assertEquals(self::DATETIME_STRING, DateTimeUtils::dateTimeString(self::DATETIME_STRING));
    }

    /**
     * @dataProvider provideRangeOfDates
     */
    public function testRangeOfDates(
        array $expected,
        string|\DateTimeInterface $from,
        string|\DateTimeInterface $to,
        array $days = [],
        string $format = 'Y-m-d',
        string $step = 'P1D',
        bool $inclusive = true,
    ) {
        $this->assertEquals($expected, DateTimeUtils::getRangeOfDates($from, $to, $days, $format, $step, $inclusive));
    }

    /**
     * @dataProvider provideIntervalOfDatesData
     */
    public function testIntervalOfDates($expected, string|\DateTimeInterface $from, string|\DateTimeInterface $to)
    {
        $this->assertEquals($expected, DateTimeUtils::intervalOfDates($from, $to));
    }

    public static function provideIntervalOfDatesData(): \Generator
    {
        yield [
            5,
            '2022-02-02',
            '2022-02-07',
        ];
        yield [
            5,
            new \DateTime('2022-02-02'),
            new \DateTime('2022-02-07'),
        ];
    }

    public function testCheckDateTime()
    {
        $this->assertNull(DateTimeUtils::checkDateTime(null));
        $this->assertInstanceOf(\DateTimeInterface::class, DateTimeUtils::checkDateTime('today'));
        $this->assertInstanceOf(\DateTimeInterface::class, DateTimeUtils::checkDateTime(self::DATE_STRING));
        $this->assertInstanceOf(\DateTimeInterface::class, DateTimeUtils::checkDateTime(self::DATETIME_STRING));
    }

    public function provideRangeOfDates(): \Generator
    {
        yield [
            ['2022-02-02'],
            '2022-02-02',
            '2022-02-02',
        ];
        yield [
            ['2022-02-02', '2022-02-03', '2022-02-04'],
            '2022-02-02',
            '2022-02-04',
        ];
        yield [
            ['2022-02-02', '2022-02-03', '2022-02-04'],
            new \DateTime('2022-02-02'),
            new \DateTime('2022-02-04'),
        ];
        yield [
            ['2022-02-02', '2022-02-03', '2022-02-04', '2022-02-07', '2022-02-08'],
            '2022-02-02',
            '2022-02-09',
            ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'],
            'Y-m-d',
            'P1D',
            false,
        ];
    }

    public function testGetWeekdays()
    {
        $this->assertEquals([
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ], DateTimeUtils::getWeekdays());
        $this->assertEquals([], DateTimeUtils::getWeekdays([]));
        $this->assertEquals([0, 1, 2], DateTimeUtils::getWeekdays(['monday', 'tuesday', 'Wednesday']));
        $this->assertEquals([6, 3, 4], DateTimeUtils::getWeekdays(['sunday', 'THURSDAY', 'friday']));
        $this->assertEquals([false, 3, 4], DateTimeUtils::getWeekdays(['invalid', 'thursday', 'friday']));
    }

    public function testGetWorkdays()
    {
        $this->assertEquals([
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
        ], DateTimeUtils::getWorkdays());
    }

    public function testDateIsValid()
    {
        $this->assertTrue(DateTimeUtils::isDateValid('00:00', 'H:i'));
        $this->assertFalse(DateTimeUtils::isDateValid('24:09', 'H:i'));
        $this->assertFalse(DateTimeUtils::isDateValid('25:00', 'H:i'));
    }

    public function testIsDateStringValid()
    {
        $this->assertTrue(DateTimeUtils::isDateStringValid('10 DAY'));
    }

    public function testIsBetween()
    {
        $this->assertTrue(DateTimeUtils::isBetween('10:00', '18:00', '12:00'));
        $this->assertFalse(DateTimeUtils::isBetween('10:00', '18:00', '22:00'));
        $this->assertTrue(DateTimeUtils::isBetween('2022-02-02', '2022-02-03', '2022-02-02', '!Y-m-d'));
        $this->assertFalse(
            DateTimeUtils::isBetween(
                '2022-02-02 10:00:00',
                '2022-02-02 18:00:00',
                '2022-02-02 22:00:00',
                '!Y-m-d H:i:s',
            ),
        );
    }

    public function testIsDateStringValidPluralPresenter()
    {
        $this->assertTrue(DateTimeUtils::isDateStringValid('10 HOURS'));
    }

    public function testIsDateStringValidNoInterval()
    {
        $this->assertFalse(DateTimeUtils::isDateStringValid('MONTH'));
    }

    public function testIsDateStringValidNoIntervalPresenter()
    {
        $this->assertFalse(DateTimeUtils::isDateStringValid('10'));
    }

    public function testIsDateStringValidInvalidInterval()
    {
        $this->assertFalse(DateTimeUtils::isDateStringValid('10,3 YEAR'));
        $this->assertFalse(DateTimeUtils::isDateStringValid('10.3 YEAR'));
    }

    public function testHumanizeSeconds()
    {
        DateTimeUtils::startTime();

        sleep(3);

        DateTimeUtils::stopTime();

        $this->assertEquals('3 seconds', DateTimeUtils::duration());
    }

    public function testHumanizeSecondsZeroTime()
    {
        DateTimeUtils::startTime();

        DateTimeUtils::stopTime();

        $this->assertEquals('0 second', DateTimeUtils::duration());
    }

    public function testHumanizeSecondsCustomStartAndStop()
    {
        $start = microtime(true);

        sleep(3);

        $stop = microtime(true);

        $this->assertEquals('3 seconds', DateTimeUtils::duration(true, $start, $stop));
    }

    public function testHumanizeSecondsDisableHumanize()
    {
        $start = microtime(true);

        sleep(2);

        $stop = $start + 2;

        $this->assertEquals('2', DateTimeUtils::duration(false, $start, $stop));
    }
}
