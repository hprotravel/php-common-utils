<?php

namespace Tests;

use Compass\Utils\ObjectUtils;
use Tests\Fixtures\DummyHlp;

class ObjectUtilsTest extends UtilityTestCase
{

    /**
     * @dataProvider provideClassBasenameEqualsData
     */
    public function testClassBasenameEquals(string $expected, $input)
    {
        $this->assertEquals($expected, ObjectUtils::classBasename($input));
    }

    public function provideClassBasenameEqualsData(): \Generator
    {
        yield ['DummyHlp', DummyHlp::class];
        yield ['DummyHlp', new DummyHlp()];
    }

    /**
     * @dataProvider provideClassBasenameNotEqualsData
     */
    public function testClassBasenameNotEquals(string $expected, $input)
    {
        $this->assertNotEquals($expected, class_basename($input));
    }

    public function provideClassBasenameNotEqualsData(): \Generator
    {
        yield ['DummyHlp', new \stdClass()];
        yield [
            'DummyHlp',
            new class {},
        ];
    }

    public function testClassHeadline()
    {
        $this->assertEquals('Dummy Hlp', ObjectUtils::classHeadline(DummyHlp::class));
    }

}