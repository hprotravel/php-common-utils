<?php

namespace Tests;

use Compass\Utils\IpUtils;

class IpUtilsTest extends UtilityTestCase
{

    public function providerIpV4(): array
    {
        return [
            ['127.0.0.1', true],
            ['000.12.12.034', false],
            ['000.12.234.23.23', false],
            ['121.234.12.12', true],
            ['I.Am.not.an.ip', false],
        ];
    }

    public function providerIpV6(): array
    {
        return [
            ['d603:840b:4bdb:1961:92d4:ae00:9d1b:b819', true],
            ['25ad:4349:b083:58f2:1a8c:e9f6:358f', false],
            ['3738:d043:0bf6:9dd3:afc9:df7a:156d:ca53', true],
            ['a96d:0ecd:bb78:7fb5:e110:17df:626f:', false],
            ['I:am:not:an:ip:address:too:bro', false],
        ];
    }

    /**
     * @dataProvider providerIpV4
     * @dataProvider providerIpV6
     */
    public function testCheckIp(string $ip, bool $expected): void
    {
        $this->assertEquals($expected, IpUtils::checkIp($ip));
    }

    /**
     * @dataProvider providerIpV4
     */
    public function testCheckIpV4(string $ip, bool $expected): void
    {
        $this->assertEquals($expected, IpUtils::checkIpV4($ip));
    }

    /**
     * @dataProvider providerIpV6
     */
    public function testCheckIpV6(string $ip, bool $expected): void
    {
        $this->assertEquals($expected, IpUtils::checkIpV6($ip));
    }
}