<?php

namespace Compass\Utils;

use InvalidArgumentException;

final class ArrayUtils extends AbstractUtils
{
    public const CLOSEST_METHOD_HIGHER = 'higher';
    public const CLOSEST_METHOD_CLOSEST = 'closest';
    public const CLOSEST_METHOD_LOWER = 'lower';

    private const CLOSEST_METHODS = [
        self::CLOSEST_METHOD_HIGHER,
        self::CLOSEST_METHOD_CLOSEST,
        self::CLOSEST_METHOD_LOWER,
    ];

    /**
     * Determine if the given key exists in the provided array.
     */
    public static function exists(array $array, int|string $key): bool
    {
        return \array_key_exists($key, $array);
    }

    /**
     * Get all the given array except for a specified array of keys.
     */
    public static function except(array $array, array|string $keys): array
    {
        ArrayUtils::forget($array, $keys);

        return $array;
    }

    /**
     * Return the first element in an array passing a given truth test.
     */
    public static function first(array $array, ?callable $callback = null, mixed $default = null): mixed
    {
        if (\is_null($callback)) {
            if (empty($array)) {
                return \value($default);
            }

            foreach ($array as $item) {
                return $item;
            }
        }

        foreach ($array as $key => $value) {
            if ($callback($value, $key)) {
                return $value;
            }
        }

        return \value($default);
    }

    /**
     * Return the last element in an array passing a given truth test.
     */
    public static function last(array $array, ?callable $callback = null, mixed $default = null): mixed
    {
        if (\is_null($callback)) {
            return empty($array) ? \value($default) : \end($array);
        }

        return ArrayUtils::first(\array_reverse($array, true), $callback, $default);
    }

    /**
     * Get an item from an array using "dot" notation.
     */
    public static function get(array $array, int|string|null $key, mixed $default = null): mixed
    {
        if (!ArrayUtils::accessible($array)) {
            return \value($default);
        }

        if (\is_null($key)) {
            return $array;
        }

        if (ArrayUtils::exists($array, $key)) {
            return $array[$key];
        }

        if (!\str_contains($key, '.')) {
            return $array[$key] ?? \value($default);
        }

        foreach (\explode('.', $key) as $segment) {
            if (ArrayUtils::accessible($array) && ArrayUtils::exists($array, $segment)) {
                $array = $array[$segment];
            } else {
                return \value($default);
            }
        }

        return $array;
    }

    /**
     * Remove one or many array items from a given array using "dot" notation.
     */
    public static function forget(array &$array, array|string $keys): void
    {
        $original = &$array;

        $keys = (array)$keys;

        if (0 === \count($keys)) {
            return;
        }

        foreach ($keys as $key) {
            // if the exact key exists in the top-level, remove it
            if (ArrayUtils::exists($array, $key)) {
                unset($array[$key]);

                continue;
            }

            $parts = \explode('.', $key);

            // clean up before each pass
            $array = &$original;

            while (\count($parts) > 1) {
                $part = \array_shift($parts);

                if (isset($array[$part]) && \is_array($array[$part])) {
                    $array = &$array[$part];
                } else {
                    continue 2;
                }
            }

            unset($array[\array_shift($parts)]);
        }
    }

    /**
     * Check if an item or items exist in an array using "dot" notation.
     */
    public static function has(array $array, array|string $keys): bool
    {
        $keys = (array)$keys;

        if (!$array || [] === $keys) {
            return false;
        }

        foreach ($keys as $key) {
            $subKeyArray = $array;

            if (ArrayUtils::exists($array, $key)) {
                continue;
            }

            foreach (\explode('.', $key) as $segment) {
                if (ArrayUtils::accessible($subKeyArray) && ArrayUtils::exists($subKeyArray, $segment)) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    public static function group(array $array, int|string $key): array
    {
        $group = [];
        foreach ($array as $value) {
            if (\array_key_exists($key, $value)) {
                $group[$value[$key]][] = $value;
            }
        }

        return $group;
    }

    /**
     * Determines if an array is associative.
     *
     * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
     */
    public static function assoc(array $array): bool
    {
        $keys = \array_keys($array);

        return \array_keys($keys) !== $keys;
    }

    /**
     * Determines if an array is consecutive.
     *
     * In the context of an array, "consecutive" usually refers to elements that appear one after
     * the other in a sequence without any gaps.
     */
    public static function consec(array $array, $interval = null): bool
    {
        if (empty($array)) {
            return false;
        }

        $array = \array_values($array);

        $prev = null;
        foreach ($array as $i => $next) {
            if (null !== $prev && null !== $interval && \is_numeric($next)) {
                if (false === ($prev + $interval == $next)) {
                    return false;
                }
            } elseif (null !== $prev && $next <= $prev) {
                return false;
            }

            $prev = $next;
        }

        return true;
    }

    /**
     * Get a subset of the items from the given array.
     */
    public static function only(array $array, array|string $keys): array
    {
        return \array_intersect_key($array, \array_flip((array)$keys));
    }

    /**
     * Flatten a multi-dimensional associative array with dots.
     */
    public static function dot(array $array, string $prepend = ''): array
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (\is_array($value) && !empty($value)) {
                $results = \array_merge($results, ArrayUtils::dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }

    public static function undot(iterable $array): array
    {
        $results = [];

        foreach ($array as $key => $value) {
            ArrayUtils::set($results, $key, $value);
        }

        return $results;
    }

    /**
     * Push an item onto the beginning of an array.
     */
    public static function prepend(array $array, mixed $value, mixed $key = null): array
    {
        if (2 == \func_num_args()) {
            \array_unshift($array, $value);
        } else {
            $array = [$key => $value] + $array;
        }

        return $array;
    }

    /**
     * Get a \value from the array, and remove it.
     */
    public static function pull(array &$array, mixed $key, mixed $default = null): mixed
    {
        $value = ArrayUtils::get($array, $key, $default);

        ArrayUtils::forget($array, $key);

        return $value;
    }

    /**
     * Get one or a specified number of random values from an array.
     *
     * @throws InvalidArgumentException
     */
    public static function random(array $array, ?int $number = null, bool $preserveKeys = false): mixed
    {
        $requested = \is_null($number) ? 1 : $number;

        $count = \count($array);

        if ($requested > $count) {
            throw new InvalidArgumentException(
                \sprintf('You requested %d items, but there are only %d items available.', $requested, $count)
            );
        }

        if (\is_null($number)) {
            return $array[\array_rand($array)];
        }

        if (0 === (int)$number) {
            return [];
        }

        $keys = \array_rand($array, $number);

        $results = [];

        if ($preserveKeys) {
            foreach ((array)$keys as $key) {
                $results[$key] = $array[$key];
            }
        } else {
            foreach ((array)$keys as $key) {
                $results[] = $array[$key];
            }
        }

        return $results;
    }

    /**
     * Set an array item to a given \value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     */
    public static function set(array &$array, mixed $key, mixed $value): array
    {
        if (\is_null($key)) {
            return $array = $value;
        }

        $keys = \explode('.', $key);

        foreach ($keys as $i => $key) {
            if (1 === \count($keys)) {
                break;
            }

            unset($keys[$i]);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next \value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($array[$key]) || !\is_array($array[$key])) {
                $array[$key] = [];
            }

            $array = &$array[$key];
        }

        $array[\array_shift($keys)] = $value;

        return $array;
    }

    /**
     * Shuffle the given array and return the result.
     */
    public static function shuffle(array $array, ?int $seed = null): array
    {
        if (\is_null($seed)) {
            \shuffle($array);
        } else {
            \mt_srand($seed);
            \shuffle($array);
            \mt_srand();
        }

        return $array;
    }

    /**
     * Flatten a multi-dimensional array into a single level.
     */
    public static function flatten(array $array, float|int $depth = INF): array
    {
        $result = [];

        foreach ($array as $item) {
            $item = $item instanceof Collection ? $item->all() : $item;

            if (!\is_array($item)) {
                $result[] = $item;
            } else {
                $values = 1 === $depth
                    ? \array_values($item)
                    : ArrayUtils::flatten($item, $depth - 1);

                foreach ($values as $value) {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     */
    public static function add(array $array, mixed $key, mixed $value): array
    {
        if (\is_null(ArrayUtils::get($array, $key))) {
            ArrayUtils::set($array, $key, $value);
        }

        return $array;
    }

    public static function sortBy(array $data, array|string $key, string $direction = 'asc'): array
    {
        $key = (array)$key;

        \usort(
            $data,
            function ($a, $b) use ($key, $direction) {
                $result = 0;
                foreach ($key as $name) {
                    if ($result != 0) {
                        continue;
                    }

                    $result = ($direction == 'asc')
                        ? \strnatcmp($a[$name], $b[$name])
                        : \strnatcmp($b[$name], $a[$name]);
                }

                return $result;
            }
        );

        return $data;
    }

    public static function list(int $min, int $max): array
    {
        $list = [];
        foreach (\range($min, $max) as $index) {
            $list[$index] = $index;
        }

        return $list;
    }

    /**
     * Merge one or more arrays.
     */
    public static function replace(array ...$arrays): array
    {
        return \array_replace(...$arrays);
    }

    /**
     * Get an array depth.
     */
    public static function depth(array $array): int
    {
        $maxDepth = 1;
        foreach ($array as $value) {
            if (\is_array($value)) {
                $depth = ArrayUtils::depth($value) + 1;

                if ($depth > $maxDepth) {
                    $maxDepth = $depth;
                }
            }
        }

        return $maxDepth;
    }

    /**
     * Convert the array into a query string.
     */
    public static function query(array $array): string
    {
        return \http_build_query($array, '', '&', PHP_QUERY_RFC3986);
    }

    /**
     * Filter the array using the given callback.
     */
    public static function where(array $array, callable $callback): array
    {
        return \array_filter($array, $callback, ARRAY_FILTER_USE_BOTH);
    }

    public static function search(array $array, int|string $key, mixed $value): array
    {
        if (\array_key_exists($key, $array) && $array[$key] == $value) {
            return $array;
        }

        $results = [];

        foreach ($array as $subArray) {
            if (\is_array($subArray)) {
                $results = \array_merge($results, ArrayUtils::search($subArray, $key, $value));
            }
        }

        return $results;
    }

    /**
     * Get an array with the number from $array: lower, higher and closest to $value
     */
    public static function closest(
        array $array,
        mixed $value,
        string $method = ArrayUtils::CLOSEST_METHOD_CLOSEST
    ): int|array {
        if (!\in_array($method, ArrayUtils::CLOSEST_METHODS)) {
            throw new \InvalidArgumentException(
                \sprintf(
                    'The "%s" is not a valid argument. Available arguments are "%s"',
                    $method,
                    \implode(', ', ArrayUtils::CLOSEST_METHODS)
                )
            );
        }

        // Sorts the array from lowest to highest
        \sort($array);

        // Sets the array that will be returned, initially with the lowest and highest number from $array
        $result = [
            ArrayUtils::CLOSEST_METHOD_LOWER => \min(\current($array), $value),
            ArrayUtils::CLOSEST_METHOD_HIGHER => \max(\end($array), $value),
            ArrayUtils::CLOSEST_METHOD_CLOSEST => $value,
        ];

        // Traverse the numbers, stores in $result the number immediately lower and higher than $value
        foreach ($array as $num) {
            if ($value > $num) {
                $result[ArrayUtils::CLOSEST_METHOD_LOWER] = $num;
            } elseif ($value <= $num) {
                // If the current number from $array is equal to $value, or immediately higher, stores that number
                // and stops the foreach() loop
                $result[ArrayUtils::CLOSEST_METHOD_HIGHER] = $num;
                break;
            }
        }

        // Here it gets the closest number to $value
        // (the number ('lower' or 'higher') with the lowest difference between its value and $value)
        if (\abs($value - $result[ArrayUtils::CLOSEST_METHOD_LOWER])
            < \abs($result[ArrayUtils::CLOSEST_METHOD_HIGHER] - $value)
        ) {
            $result[ArrayUtils::CLOSEST_METHOD_CLOSEST] = $result[ArrayUtils::CLOSEST_METHOD_LOWER];
        } else {
            $result[ArrayUtils::CLOSEST_METHOD_CLOSEST] = $result[ArrayUtils::CLOSEST_METHOD_HIGHER];
        }

        return $result[$method];
    }

    /**
     * Removes empty arrays.
     */
    public static function removeEmpty(array $array): array
    {
        foreach ($array as $key => $value) {
            if (\is_array($value)) {
                $array[$key] = ArrayUtils::removeEmpty($value);
            }

            if (empty($array[$key])) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public static function inArrayRecursive(string $needle, array $haystack, bool $strict = false): bool
    {
        foreach ($haystack as $item) {
            if (
                ($strict ? $item === $needle : $item == $needle)
                || (\is_array($item) && self::inArrayRecursive($needle, $item, $strict))
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sort an array recursively by key.
     */
    public static function ksortRecursive(mixed &$array, int $sortFlags = SORT_REGULAR): bool
    {
        if (!\is_array($array)) {
            return false;
        }

        \ksort($array, $sortFlags);

        foreach ($array as $key => $value) {
            if (\is_array($value)) {
                ArrayUtils::ksortRecursive($array[$key]);
            }
        }

        return true;
    }

    /**
     * Determine whether a variable is considered to be empty recursively.
     * A variable is considered empty if it does not exist or if its \value.
     */
    public static function emptyRecursive(mixed $array): bool
    {
        $result = true;

        if (\is_array($array) && \count($array) > 0) {
            foreach ($array as $item) {
                $result = $result && ArrayUtils::emptyRecursive($item);
            }
        } else {
            $result = empty($array);
        }

        return $result;
    }

    /**
     * Returns an array with the differences between $source and $target
     */
    public static function diffAssocRecursive(array $source, array $target): array
    {
        $results = [];
        foreach ($source as $key => $value) {
            if (!\array_key_exists($key, $target)) {
                $results[$key] = $value;

                continue;
            }

            if (\is_array($value)) {
                $recursiveArrayDiff = ArrayUtils::diffAssocRecursive($value, $target[$key]);
                if (\count($recursiveArrayDiff)) {
                    $results[$key] = $recursiveArrayDiff;
                }

                continue;
            }

            if ($value != $target[$key]) {
                $results[$key] = $value;
            }
        }

        return $results;
    }

    public static function mapWithKeys(array $array, callable $callback): array
    {
        $ks = \array_keys($array);
        $fx = function ($key) use ($array, $callback) {
            return [$key => $callback($key, $array[$key])];
        };

        return \array_combine($ks, ArrayUtils::flatten(\array_map($fx, $ks), 1));
    }

    public static function implodeWithKeys(string $glue, array $array): string
    {
        return \implode($glue, ArrayUtils::mapWithKeys($array, function ($k, $v) {
            return \sprintf("%s='%s'", $k, $v);
        }));
    }

    /**
     * If the given \value is not an array and not null, wrap it in one.
     */
    public static function wrap(mixed $value): array
    {
        if (\is_null($value)) {
            return [];
        }

        return \is_array($value) ? $value : [$value];
    }

    /**
     * Determine whether the given \value is array-accessible.
     */
    public static function accessible(mixed $value): bool
    {
        return \is_array($value);
    }

    /**
     * Generate all the possible permutations among a set of nested arrays.
     */
    public static function permutation(array $arrays): array
    {
        $result = [[]];
        foreach ($arrays as $property => $propertyValues) {
            $tmp = [];
            foreach ($result as $resultItem) {
                foreach ($propertyValues as $propertyValue) {
                    $tmp[] = \array_merge($resultItem, [$property => $propertyValue]);
                }
            }
            $result = $tmp;
        }

        return $result;
    }

    public static function permute(array $arrays): array
    {
        $result = [];

        $permute = function ($currentIndex, $currentPermutation) use (&$permute, &$result, $arrays) {
            if ($currentIndex == \count($arrays)) {
                $result[] = $currentPermutation;

                return;
            }

            foreach ($arrays[$currentIndex] as $value) {
                $newPermutation = $currentPermutation;
                $newPermutation[] = $value;
                $permute($currentIndex + 1, $newPermutation);
            }
        };

        $permute(0, []);

        return \array_values($result);
    }

    public static function combine(array $arrays): array
    {
        $result = [];

        $combine = function ($currentIndex, $currentCombination) use (&$combine, &$result, $arrays) {
            if ($currentIndex == \count($arrays)) {
                \sort($currentCombination);
                $key = \implode(',', $currentCombination);

                if (!isset($result[$key])) {
                    $result[$key] = $currentCombination;
                }

                return;
            }

            foreach ($arrays[$currentIndex] as $value) {
                $newCombination = $currentCombination;
                $newCombination[] = $value;
                $combine($currentIndex + 1, $newCombination);
            }
        };

        $combine(0, []);

        return \array_values($result);
    }

    public static function join(array $array, string $separator = ','): string
    {
        return \implode($separator, $array);
    }
}
