<?php

namespace Compass\Utils;

class Env
{
    const ENV_KEY = 'APP_ENV';

    public static function get(): ?string
    {
        if (false === $appEnv = \getenv(self::ENV_KEY)) {
            return $_SERVER[self::ENV_KEY] ?? $_ENV[self::ENV_KEY] ?? Environment::TEST->get();
        }

        return $appEnv;
    }

    public static function isDevelopment(): bool
    {
        return Environment::tryFrom(self::get())->is(Environment::DEV);
    }

    public static function isTesting(): bool
    {
        return Environment::tryFrom(self::get())->is(Environment::TEST);
    }

    public static function isStaging(): bool
    {
        return Environment::tryFrom(self::get())->is(Environment::STAG);
    }

    public static function isProduction(): bool
    {
        return Environment::tryFrom(self::get())->is(Environment::PROD);
    }
}