<?php

namespace Compass\Utils;

final class CreditCardUtils extends AbstractUtils
{
    public static function maskCardNumber(string $number): string
    {
        return StringUtils::replaceCharWith($number, 0, 12);
    }

    public static function maskCardSecurityCode(string $number): string
    {
        return StringUtils::replaceCharWith($number, 0, 3);
    }
}