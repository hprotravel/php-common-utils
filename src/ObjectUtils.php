<?php

namespace Compass\Utils;

final class ObjectUtils extends AbstractUtils
{
    public static function classBasename($class): string
    {
        $class = \is_object($class) ? \get_class($class) : $class;

        return \basename(\str_replace('\\', '/', $class));
    }

    public static function classHeadline(string $class): string
    {
        return \headline(self::classBasename($class));
    }

}
