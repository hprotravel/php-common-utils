<?php

namespace Compass\Utils;

use Random\Randomizer;
use Traversable;

final class StringUtils extends AbstractUtils
{
    /**
     * The cache of snake-cased words.
     */
    private static array $snakeCache = [];

    /**
     * The cache of camel-cased words.
     */
    private static array $camelCache = [];

    /**
     * The cache of studly-cased words.
     */
    private static array $studlyCache = [];

    public static function empty(string $value, bool $strict = false): bool
    {
        if ($strict) {
            $value = \trim($value);

            return '' === $value;
        }

        return '' == $value;
    }

    /**
     * Generate random string.
     */
    public static function random(int $length = 8): string
    {
        return \substr(\bin2hex((new Randomizer())->getBytes($length)), 0, $length);
    }

    /**
     * Determine if a given string starts with a given substring.
     */
    public static function startsWith(string $haystack, array|string $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if (\str_starts_with($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a given string ends with a given substring.
     */
    public static function endsWith(string $haystack, array|string $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if (\str_ends_with($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a given string contains a given substring.
     */
    public static function contains(string $haystack, array|string $needles): bool
    {
        foreach ((array)$needles as $needle) {
            if (\str_contains($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Generate a slug.
     */
    public static function slug(string $string, string $separator = '-', bool $lowerCase = true): string
    {
        // Replace non-letter or digits by dash "-"
        $string = \preg_replace('/[^\pL\d]+/u', $separator, $string);

        // Transliterate
        $string = \iconv('UTF-8', 'ASCII//TRANSLIT', $string);

        // Remove unwanted characters
        $string = \preg_replace('/[^-\w]+/', '', $string);

        // Trim
        $string = \trim($string, $separator);

        // Replace consecutive separators with a single one
        $string = \preg_replace('/'.\preg_quote($separator, '/').'{2,}/', $separator, $string);

        // Lowercase
        if ($lowerCase) {
            $string = \mb_strtolower($string);
        }

        return $string;
    }

    /**
     * Limit the number of characters in a string.
     */
    public static function limit(string $value, int $limit = 100, string $end = '...'): string
    {
        if (\mb_strwidth($value, 'UTF-8') <= $limit) {
            return $value;
        }

        return \rtrim(\mb_strimwidth($value, 0, $limit, '', 'UTF-8')).$end;
    }

    /**
     * Interpolate a message with context.
     */
    public static function interpolate(string $message, array $context = []): string
    {
        // build a replacement array with braces around the context keys
        $replace = [];
        foreach ($context as $key => $value) {
            // check that the value can be cast to string
            if (!\is_array($value) && (!\is_object($value) || \method_exists($value, '__toString'))) {
                $replace['{'.$key.'}'] = $value;
            }
        }

        // interpolate replacement values into the message and return
        return \strtr($message, $replace);
    }

    public static function hash(mixed $value, string $algo = 'md5', bool $raw = false): string
    {
        return \hash($algo, \serialize($value), $raw);
    }

    /**
     * Remove double quotes or single quotes from a string.
     */
    public static function stripQuotes(string $string): array|string|null
    {
        return \preg_replace('/^(\'(.*)\'|"(.*)")$/', '$2$3', $string);
    }

    public static function lowerCase(?string $string): string
    {
        return \mb_convert_case($string ?? '', MB_CASE_LOWER, 'UTF-8');
    }

    public static function upperCase(?string $string): string
    {
        return \mb_convert_case($string ?? '', MB_CASE_UPPER, 'UTF-8');
    }

    public static function titleCase(?string $string): string
    {
        return \mb_convert_case($string ?? '', MB_CASE_TITLE, 'UTF-8');
    }

    /**
     * Convert a string to snake case.
     */
    public static function snakeCase(string $value, string $delimiter = '_'): string
    {
        $key = $value;

        if (isset(StringUtils::$snakeCache[$key][$delimiter])) {
            return StringUtils::$snakeCache[$key][$delimiter];
        }

        if (!\ctype_lower($value)) {
            $value = \preg_replace('/\s+/u', '', ucwords($value));

            $value = StringUtils::lowerCase(\preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
        }

        return StringUtils::$snakeCache[$key][$delimiter] = $value;
    }

    /**
     * Convert a value to camel case.
     */
    public static function camelCase(string $value): string
    {
        if (isset(StringUtils::$camelCache[$value])) {
            return StringUtils::$camelCache[$value];
        }

        return StringUtils::$camelCache[$value] = \lcfirst(StringUtils::studlyCase($value));
    }

    /**
     * Convert a value to studly caps case.
     */
    public static function studlyCase(string $value): string
    {
        $key = $value;

        if (isset(StringUtils::$studlyCache[$key])) {
            return StringUtils::$studlyCache[$key];
        }

        $words = \explode(' ', StringUtils::replace(['-', '_'], ' ', $value));

        $studlyWords = \array_map(fn($word) => StringUtils::ucfirst($word), $words);

        return StringUtils::$studlyCache[$key] = \implode($studlyWords);
    }

    /**
     * Convert the given string to title case for each word.
     */
    public static function headline(string $value): string
    {
        $parts = \explode(' ', $value);

        $parts = \count($parts) > 1
            ? \array_map([StringUtils::class, 'titleCase'], $parts)
            : \array_map([StringUtils::class, 'titleCase'], StringUtils::ucsplit(\implode('_', $parts)));

        $collapsed = StringUtils::replace(['-', '_', ' '], '_', \implode('_', $parts));

        return \implode(' ', \array_filter(\explode('_', $collapsed)));
    }

    /**
     * Split a string into pieces by uppercase characters.
     */
    public static function ucsplit(string $string): array
    {
        return \preg_split('/(?=\p{Lu})/u', $string, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Make a string's first character uppercase.
     */
    public static function ucfirst(string $string): string
    {
        return StringUtils::upperCase(\substr($string, 0, 1)).\substr($string, 1);
    }

    /**
     * Returns the portion of the string specified by the start and length parameters.
     */
    public static function substr(string $string, int $start, ?int $length = null, string $encoding = 'UTF-8'): string
    {
        return \mb_substr($string, $start, $length, $encoding);
    }

    /**
     * Replace the given value in the given string.
     */
    public static function replace(
        iterable|string $search,
        iterable|string $replace,
        iterable|string $subject,
        bool $caseSensitive = true
    ): array|string {
        if ($search instanceof Traversable) {
            $search = \collect($search)->all();
        }

        if ($replace instanceof Traversable) {
            $replace = \collect($replace)->all();
        }

        if ($subject instanceof Traversable) {
            $subject = \collect($subject)->all();
        }

        return $caseSensitive
            ? \str_replace($search, $replace, $subject)
            : \str_ireplace($search, $replace, $subject);
    }

    /**
     * Replace char(s) by any character.
     */
    public static function replaceCharWith(
        string $string = '',
        int $first = 0,
        int $last = 0,
        string $replace = '*'
    ): string {
        if ($last <= 0) {
            $last = \strlen($string) + $last;

            if ($first < 0) {
                $first = \strlen($string) + $first;
            }
        }

        $begin = \substr($string, 0, $first);
        $middle = \str_repeat($replace, \strlen(\substr($string, $first, $last - $first)));
        $end = \substr($string, $last);

        return $begin.$middle.$end;
    }

    /**
     * Linefeed means to advance downward to the next line; however, it has been repurposed and renamed.
     * Used as "newline", it terminates lines (commonly confused with separating lines).
     * This is commonly escaped as "\n", abbreviated LF or NL, and has ASCII value 10 or 0xA.
     * CRLF (but not CRNL) is used for the pair "\r\n".
     */
    public static function removeLineFeed(string $query): string
    {
        return \preg_replace('/(\r\n|\r|\n)/', '', $query);
    }

    public static function removeSuffix(string $string, string $needle): string
    {
        if (\str_ends_with($string, $needle)) {
            $string = \substr($string, 0, -\strlen($needle));
        }

        return $string;
    }

    public static function removePrefix(string $string, string $needle): string
    {
        if (\str_starts_with($string, $needle)) {
            $string = \substr($string, \strlen($needle));
        }

        return $string;
    }

    public static function split(string $string, string $separator = ',:|'): array|false
    {
        return \preg_split("/[$separator]/", $string);
    }

    public static function escape(string $string, $charset = 'UTF-8'): string
    {
        return \htmlspecialchars($string, \ENT_QUOTES | \ENT_SUBSTITUTE, $charset);
    }
}
