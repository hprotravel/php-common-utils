<?php

namespace Compass\Utils;

interface Arrayable
{
    public function toArray(): array;
}