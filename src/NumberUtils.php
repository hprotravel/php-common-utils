<?php

namespace Compass\Utils;

final class NumberUtils extends AbstractUtils
{
    public static function format(float $number): string
    {
        return \number_format($number, 2, '.', '');
    }

    public static function ordinal(float $number): string
    {
        $ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];

        if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
            return $number.'th';
        } else {
            return $number.$ends[$number % 10];
        }
    }

    public static function between(int $start, int $end, ?int $count = null, int $step = 1): array
    {
        if ($start > $end) {
            // Swap values if $start is greater than $end
            [$start, $end] = [$end, $start];
        }

        $result = \range($start, $end, $step);

        // Shuffle the array
        \shuffle($result);

        // Limit the result array if $limit is specified
        if ($count !== null) {
            $result = \array_slice($result, 0, $count);
        }

        // Sort the result array
        \sort($result);

        return $result;
    }

    public static function findPercent(int|float $value, int|float $result): float|int
    {
        return (($result - $value) * 100) / $value;
    }

    public static function applyPercent(int|float $value, int|float $percent, int|float &$result = 0): float|int
    {
        $result = ($value * $percent) / 100;

        return $value + $result;
    }

    public static function roundPercent(int|float $value, int|float $percentage, int $precision = 2): float
    {
        return round(self::applyPercent($value, $percentage), $precision);
    }
}