<?php

namespace Compass\Utils;

class Collection
{
    /**
     * The items contained in the collection.
     */
    protected array $items = [];

    /**
     * Create a new collection.
     */
    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    /**
     * Create a new collection.
     */
    public static function make(array $items = []): Collection
    {
        return new static($items);
    }

    /**
     * Sort through each item with a callback.
     */
    public function sort(callable|int|null $callback = null): Collection
    {
        $items = $this->items;

        $callback && \is_callable($callback)
            ? \uasort($items, $callback)
            : \asort($items);

        return new static($items);
    }

    /**
     * Sort the collection keys.
     */
    public function sortKeys(int $options = SORT_REGULAR, bool $descending = false): Collection
    {
        $items = $this->items;

        $descending ? \krsort($items, $options) : ksort($items, $options);

        return new static($items);
    }

    /**
     * Create a collection with the given \range.
     */
    public function range(int $from, int $to, int $step = 1): Collection
    {
        return new static(\range($from, $to, $step));
    }

    /**
     * Get all the items in the collection.
     */
    public function all(bool $preserveKeys = true): array
    {
        if (!$preserveKeys) {
            return \array_values($this->items);
        }

        return $this->items;
    }

    /**
     * Join all items from the collection using a string.
     */
    public function join(string $separator = ','): string
    {
        return ArrayUtils::join($this->items, $separator);
    }

    /**
     * Get the keys of the collection items.
     */
    public function keys(): Collection
    {
        return new static(\array_keys($this->items));
    }

    /**
     * Determine if an item exists in the collection.
     */
    public function contains(mixed $key, bool $strict = false): bool
    {
        return \in_array($key, $this->items, $strict);
    }

    /**
     * Search the collection for a given value and return the corresponding key if successful.
     */
    public function search(mixed $value, bool $strict = false): string|int|false
    {
        if (!$this->useAsCallable($value)) {
            return \array_search($value, $this->items, $strict);
        }

        foreach ($this->items as $key => $item) {
            if ($value($item, $key)) {
                return $key;
            }
        }

        return false;
    }

    /**
     * Get the items in the collection that are not present in the given items.
     */
    public function diff(array $items, callable $callback): Collection
    {
        return new static(\array_udiff($this->items, $items, $callback));
    }

    /**
     * Get all items except for those with the specified keys.
     */
    public function except(array|string $keys): Collection
    {
        return new static(ArrayUtils::except($this->items, $keys));
    }

    /**
     * Get the items with the specified keys.
     */
    public function only(array|string $keys): Collection
    {
        $keys = \is_array($keys) ? $keys : \func_get_args();

        return new static(ArrayUtils::only($this->items, $keys));
    }

    /**
     * Get the first item from the collection passing the given truth test.
     */
    public function first(?callable $callback = null, mixed $default = null): mixed
    {
        return ArrayUtils::first($this->items, $callback, $default);
    }

    /**
     * Get the last item from the collection.
     */
    public function last(?callable $callback = null, mixed $default = null): mixed
    {
        return ArrayUtils::last($this->items, $callback, $default);
    }

    /**
     * Get an item from the collection by key.
     */
    public function get(mixed $key, mixed $default = null): mixed
    {
        if (\array_key_exists($key, $this->items)) {
            return $this->items[$key];
        }

        return \value($default);
    }

    /**
     * Determine if an item exists in the collection by key.
     */
    public function has(mixed $key): bool
    {
        $keys = \is_array($key) ? $key : \func_get_args();

        foreach ($keys as $key) {
            if (!\array_key_exists($key, $this->items)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Run a map over each of the items.
     */
    public function map(callable $callback): Collection
    {
        $keys = \array_keys($this->items);

        $items = \array_map($callback, $this->items, $keys);

        return new static(\array_combine($keys, $items));
    }

    /**
     * Run a filter over each of the items.
     */
    public function filter(?callable $callback = null): Collection
    {
        if ($callback) {
            return new static(ArrayUtils::where($this->items, $callback));
        }

        return new static(\array_filter($this->items));
    }

    /**
     * Count the number of items in the collection.
     */
    public function count(): int
    {
        return \count($this->items);
    }

    /**
     * Determine if the given \value is callable, but not a string.
     */
    protected function useAsCallable(mixed $value): bool
    {
        return !\is_string($value) && \is_callable($value);
    }
}
