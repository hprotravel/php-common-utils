<?php

namespace Compass\Utils;

final class UrlUtils extends AbstractUtils
{
    public static function getQueryParams(string $url): array
    {
        \parse_str(\parse_url($url, PHP_URL_QUERY) ?? '', $queryParams);

        return $queryParams;
    }

    public static function addQueryParams(string $uri, array $params): string
    {
        foreach ($params as $key => $value) {
            $uri = self::addQueryParam($uri, $key, $value);
        }

        return $uri;
    }

    public static function addQueryParam(string $url, string $param, mixed $value): string
    {
        $parsedUrl = \parse_url($url);

        $url .= empty($parsedUrl['path']) ? '/' : '';
        $url .= empty($parsedUrl['query']) ? '?' : '&';
        $url .= \http_build_query([$param => $value]);

        return $url;
    }

    public static function modifyQueryParam(string $url, string $param, mixed $value): string
    {
        $value = \urlencode($value);
        $pattern = \sprintf('/(\?|&)(%s)=([^&]*)/', $param);
        $replacement = \sprintf('\1\2=%s', $value);
        $result = \preg_replace($pattern, $replacement, $url, -1, $count);

        if ($count > 0) {
            return $result;
        }

        return \sprintf("%s%s%s=%s", $url, \str_contains($url, '?') ? '&' : '?', $param, $value);
    }

    public static function removeQueryParam(string $url, string $param): string
    {
        $pattern = \sprintf('/(\?|&)(%s)=([^&]*)(&)?/', $param);

        return \preg_replace_callback(
            $pattern,
            function ($matches) {
                if (isset($matches[4])) {
                    return $matches[1];
                }

                return '';
            },
            $url
        );
    }

    public static function generateUrl(string $uri, array $params = []): string
    {
        if (!\preg_match_all('/\{([^}]+)}/', $uri, $matches)) {
            return $uri;
        }

        $attributes = $matches[1];

        $missingAttributes = \array_diff($attributes, \array_keys($params));
        if ($missingAttributes) {
            throw new \InvalidArgumentException(
                'The attributes "' . \implode('", "', $missingAttributes) . '" do not match the provided parameters.'
            );
        }

        foreach ($params as $name => $value) {
            if (\str_contains($uri, '{' . $name . '}')) {
                $uri = \str_replace('{' . $name . '}', $value, $uri);
                unset($params[$name]);
            }
        }

        return self::addQueryParams($uri, $params);
    }
}