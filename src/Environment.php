<?php

namespace Compass\Utils;

enum Environment: string
{
    case DEV = 'dev';
    case TEST = 'test';
    case STAG = 'stag';
    case PROD = 'prod';

    public function get(): string
    {
        return $this->value;
    }

    public function is(Environment $environment): bool
    {
        return $this === $environment;
    }
}