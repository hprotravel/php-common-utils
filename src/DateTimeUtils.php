<?php

namespace Compass\Utils;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeInterface;
use DateTimeZone;

final class DateTimeUtils extends AbstractUtils
{
    public const YEAR_FORMAT = 'Y';
    public const DATE_FORMAT = 'Y-m-d';
    public const TIME_FORMAT = 'H:i:s';
    public const DATETIME_FORMAT = 'Y-m-d H:i:s';
    public const DATETIME_FORMAT_EXTENDED = 'Y-m-d\TH:i:sP'; // \DateTimeInterface::ATOM

    const WEEKDAYS = [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
    ];
    const WORKDAYS = [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
    ];

    /**
     * Start time for duration.
     */
    protected static float $start;

    /**
     * Stop time for duration.
     */
    protected static float $stop;

    public static function date(string $time = 'now', ?DateTimeZone $timezone = null): DateTimeInterface
    {
        return new DateTime($time, $timezone);
    }

    public static function now(?DateTimeZone $timezone = null): DateTimeInterface
    {
        return DateTimeUtils::date(timezone: $timezone);
    }

    public static function addInterval(
        string|DateTimeInterface $date,
        string $interval = 'P1M',
        string $format = 'Y-m-d',
    ): string {
        $date = ($date instanceof DateTimeInterface) ? clone $date : new DateTime($date);

        $interval = new DateInterval($interval);

        return $date->add($interval)->format($format);
    }

    public static function subtractInterval(
        string|DateTimeInterface $date,
        string $interval = 'P1M',
        string $format = 'Y-m-d',
    ): string {
        // If $date is not an instance of DateTimeInterface, create a new DateTime object from the string
        $date = ($date instanceof DateTimeInterface) ? clone $date : new DateTime($date);

        // Create a DateInterval object
        $interval = new DateInterval($interval);

        // Subtract the interval from the date
        return $date->sub($interval)->format($format);
    }

    public static function dateString(string $time = 'now', ?DateTimeZone $timezone = null): string
    {
        return DateTimeUtils::date($time, $timezone)->format(self::DATE_FORMAT);
    }

    public static function dateTimeString(string $time = 'now'): string
    {
        return DateTimeUtils::date($time)->format(self::DATETIME_FORMAT);
    }

    public static function checkDateTime(null|string|DateTimeInterface $date): ?DateTimeInterface
    {
        return $date instanceof DateTimeInterface ? $date : ($date ? new DateTime($date) : null);
    }

    public static function getRangeOfDates(
        string|DateTimeInterface $fromDate,
        string|DateTimeInterface $toDate,
        array $validDays = [],
        string $format = self::DATE_FORMAT,
        string $step = 'P1D',
        bool $inclusive = true,
    ): array {
        $fromDate = ($fromDate instanceof DateTimeInterface) ? clone $fromDate : new DateTime($fromDate);
        $toDate = ($toDate instanceof DateTimeInterface) ? clone $toDate : new DateTime($toDate);
        $interval = new DateInterval($step);

        // Iterate from $start up to $end+1 day, one day in each iteration.
        // We add one day to the $end date, because the DatePeriod only iterates up to,
        // not including, the end date.
        if ($inclusive) {
            $toDate = $toDate->add($interval);
        }

        $dateRange = new DatePeriod($fromDate, $interval, $toDate);
        $rangeOfDate = [];

        foreach ($dateRange as $date) {
            // 'l' a full textual representation of the day of the week
            $dayTextual = \strtolower($date->format('l'));

            if (\count($validDays) > 0) {
                if (\in_array($dayTextual, $validDays)) {
                    $rangeOfDate[] = $date->format($format);
                }
            } else {
                $rangeOfDate[] = $date->format($format);
            }
        }

        return $rangeOfDate;
    }

    public static function intervalOfDates(
        string|DateTimeInterface $fromDate,
        string|DateTimeInterface $toDate,
        string $format = '%a',
    ): int {
        $fromDate = ($fromDate instanceof \DateTimeInterface) ? $fromDate : new \DateTime($fromDate);
        $toDate = ($toDate instanceof \DateTimeInterface) ? $toDate : new \DateTime($toDate);

        return (integer) $toDate->diff($fromDate)->format($format);
    }

    public static function getWeekdays(?array $days = null): array
    {
        if (null === $days) {
            return DateTimeUtils::WEEKDAYS;
        }

        return \array_map(
            function ($needle) {
                return \array_search(\strtolower($needle), self::WEEKDAYS);
            },
            $days,
        );
    }

    public static function getWorkdays(): array
    {
        return self::WORKDAYS;
    }

    public static function isDateValid(mixed $date, string $format = self::DATE_FORMAT): bool
    {
        $dateTime = DateTime::createFromFormat($format, $date);

        return $dateTime && $dateTime->format($format) === $date;
    }

    public static function isDateStringValid(string $time): bool
    {
        $parts = \explode(' ', $time);

        return \count($parts) == 2 && \ctype_digit($parts[0]) && \in_array(
                Inflector::singularize(\strtoupper($parts[1])),
                ['SECOND', 'MINUTE', 'HOUR', 'DAY', 'WEEK', 'MONTH', 'YEAR'],
            );
    }

    public static function isBetween(string $from, string $to, string $input, string $format = '!H:i'): bool
    {
        $fromDateTime = DateTime::createFromFormat($format, $from);
        $toDateTime = DateTime::createFromFormat($format, $to);
        $inputDateTime = DateTime::createFromFormat($format, $input);

        if ($fromDateTime > $toDateTime) {
            $toDateTime->modify('+1 day');
        }

        return ($fromDateTime <= $inputDateTime && $inputDateTime <= $toDateTime)
            || ($fromDateTime <= $inputDateTime->modify('+1 day') && $inputDateTime <= $toDateTime);
    }

    public static function humanizeSeconds(float $duration): string
    {
        // Ensure $duration is a non-negative integer
        $duration = \max(0, (int) $duration);

        if ($duration === 0) {
            return '0 second';
        }

        $periods = [
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1,
        ];

        $parts = [];
        foreach ($periods as $name => $dur) {
            $div = \floor($duration / $dur);

            if ($div == 0) {
                continue;
            }

            $plural = $div == 1 ? '' : 's';
            $parts[] = "$div $name$plural";

            $duration %= $dur;
        }

        $last = \array_pop($parts);

        return empty($parts) ? $last : implode(', ', $parts) . " and " . $last;
    }

    public static function startTime(): float
    {
        return DateTimeUtils::$start = \microtime(true);
    }

    public static function stopTime(): float
    {
        return DateTimeUtils::$stop = \microtime(true);
    }

    public static function duration(bool $humanize = true, ?float $start = null, ?float $stop = null): string|float
    {
        DateTimeUtils::stopTime();

        $start = $start ?? DateTimeUtils::$start;
        $stop = $stop ?? DateTimeUtils::$stop;

        $duration = $stop - $start;

        if ($humanize) {
            return DateTimeUtils::humanizeSeconds($duration);
        }

        return $duration;
    }
}