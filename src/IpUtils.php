<?php

namespace Compass\Utils;

final class IpUtils extends AbstractUtils
{
    public static function checkIp(string $ip): bool
    {
        return (bool)\filter_var($ip, FILTER_VALIDATE_IP);
    }

    public static function checkIpV4(string $ip): bool
    {
        return (bool)\filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    public static function checkIpV6(string $ip): bool
    {
        return (bool)\filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
    }
}