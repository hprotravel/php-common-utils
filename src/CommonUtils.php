<?php

namespace Compass\Utils;

class CommonUtils extends AbstractUtils
{
    /**
     * Retry an operation a given number of times.
     *
     * @throws \Exception
     */
    public static function retry(
        array|int $times,
        callable $callback,
        int|\Closure $sleepMilliseconds = 0,
        ?callable $when = null
    ): mixed {
        $attempts = 0;

        $backoff = [];

        if (\is_array($times)) {
            $backoff = $times;

            $times = \count($times) + 1;
        }

        beginning:
        $attempts++;
        $times--;

        try {
            return $callback($attempts);
        } catch (\Exception $e) {
            if ($times < 1 || ($when && !$when($e))) {
                throw $e;
            }

            $sleepMilliseconds = $backoff[$attempts - 1] ?? $sleepMilliseconds;

            if ($sleepMilliseconds) {
                \usleep(\value($sleepMilliseconds, $attempts, $e) * 1000);
            }

            goto beginning;
        }
    }
}