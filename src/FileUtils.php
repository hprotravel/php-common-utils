<?php

namespace Compass\Utils;

use Compass\Utils\Exception\FileNotFoundException;
use Compass\Utils\Exception\FileNotReadException;

class FileUtils extends AbstractUtils
{

    public static function exists(string $path): bool
    {
        return \file_exists($path);
    }

    public static function read(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to read contents.', $path));
        }

        if (false === \is_readable($path)) {
            throw new FileNotReadException(\sprintf('Cannot read content from "%s".', $path));
        }

        $content = \file_get_contents($path);

        if (false === $content) {
            throw new FileNotReadException(\sprintf('Cannot read content from "%s".', $path));
        }

        return $content;
    }

    public static function json(string $path): array
    {
        return JsonWrapper::decode(self::read($path));
    }

    public static function write(string $path, string $content): bool
    {
        return \file_put_contents($path, $content) !== false;
    }

    public static function delete(string $path): bool
    {
        return \unlink($path);
    }

    public static function copy(string $source, string $destination): bool
    {
        return \copy($source, $destination);
    }

    public static function move(string $source, string $destination): bool
    {
        return \rename($source, $destination);
    }

    public static function getMimeType(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get mime type.', $path));
        }

        return \mime_content_type($path);
    }

    public static function getExtension(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get extension.', $path));
        }

        return \pathinfo($path, \PATHINFO_EXTENSION);
    }

    public static function getFileName(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get file name.', $path));
        }

        return \pathinfo($path, \PATHINFO_FILENAME);
    }

    public static function getDirName(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get directory name.', $path));
        }

        return \pathinfo($path, \PATHINFO_DIRNAME);
    }

    public static function getBaseName(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get base name.', $path));
        }

        return \pathinfo($path, \PATHINFO_BASENAME);
    }

    public static function getRealPath(string $path): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get real path.', $path));
        }

        return \realpath($path);
    }

    public static function getFileSize(string $path): false|int
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get file size.', $path));
        }

        $size = \filesize($path);

        if ($size === false) {
            throw new FileNotReadException(\sprintf('Cannot read size from "%s".', $path));
        }

        return $size;
    }

    public static function getFileOwner(string $path): false|int
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get file owner.', $path));
        }

        $owner = \fileowner($path);

        if ($owner === false) {
            throw new FileNotReadException(\sprintf('Cannot read owner from "%s".', $path));
        }

        return $owner;
    }

    public static function getHash(string $path, string $algo = 'md5'): string
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get hash.', $path));
        }

        return \hash_file($algo, $path);
    }

    public static function getPermissions(string $path): int
    {
        if (!self::exists($path)) {
            throw new FileNotFoundException(\sprintf('Cannot access "%s" to get permissions.', $path));
        }

        $permissions = \fileperms($path);

        if ($permissions === false) {
            throw new FileNotReadException(\sprintf('Cannot read permission from "%s".', $path));
        }

        return $permissions;
    }

    public static function chmod(string $path, int $mode): bool
    {
        return \chmod($path, $mode);
    }
}