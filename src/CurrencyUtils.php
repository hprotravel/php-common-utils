<?php

namespace Compass\Utils;

final class CurrencyUtils extends AbstractUtils
{
    private static array $currencies = [];

    public static function currencies(): array
    {
        self::loadCurrencies();

        return self::$currencies;
    }

    public static function currencyCodes(): array
    {
        return \array_column(self::currencies(), 'code');
    }

    public static function currencyNames(): array
    {
        return \array_column(self::currencies(), 'name');
    }

    public static function currency(string $code): array
    {
        if (!self::currencyExists($code)) {
            throw new \InvalidArgumentException("Unknown currency code '$code'.");
        }

        return self::currencies()[$code];
    }

    public static function currencyName(string $code): string
    {
        if (!self::currencyExists($code)) {
            throw new \InvalidArgumentException("Unknown currency code '$code'.");
        }

        return self::currencies()[$code]['name'];
    }

    public static function currencySymbol(string $code): string
    {
        if (!self::currencyExists($code)) {
            throw new \InvalidArgumentException("Unknown currency code '$code'.");
        }

        return self::currencies()[$code]['symbol'];
    }

    public static function currencyExists(string $code): bool
    {
        return \array_key_exists($code, self::currencies());
    }

    public static function currencyHasSymbol(string $code): bool
    {
        return self::currencyExists($code) && self::currencySymbol($code) !== $code;
    }

    public static function round(float|int $amount, int $precision = 2): float
    {
        return \round($amount, $precision);
    }

    public static function format(float|int $amount, string $code, string $locale = 'en'): string
    {
        $formatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);

        $result = $formatter->formatCurrency($amount, $code);

        /*if (\str_contains($result, $code) && self::currencyHasSymbol($code)) {
            $result = \str_replace([$code, ' ', ' '], [self::currencySymbol($code), ''], $result);
        }*/

        // replace narrow non-breakable spaces, non-breakable spaces with space
        return \str_replace([' ', ' '], ' ', $result);
    }

    private static function loadCurrencies(): void
    {
        if ([] === self::$currencies) {
            self::$currencies = require __DIR__ . '/../resources/currencies.php';
        }
    }
}