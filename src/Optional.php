<?php

namespace Compass\Utils;

/**
 * @deprecated use PHP 8.0 Null-safe operator https://wiki.php.net/rfc/nullsafe_operator
 */
class Optional
{
    /**
     * The target being transformed
     * Use _ prefix to avoid namespace conflict on __get().
     */
    protected mixed $_target;

    /**
     * Create a new transform proxy instance.
     */
    public function __construct(mixed $_target)
    {
        $this->_target = $_target;
    }

    /**
     * Dynamically pass property fetching to the target when it's present.
     */
    public function __get(string $name)
    {
        if (\is_object($this->_target)) {
            return $this->_target->{$name};
        }
    }

    /**
     * Dynamically pass method calls to the target when it's present.
     */
    public function __call(string $name, array $arguments)
    {
        if (\is_object($this->_target)) {
            return $this->_target->{$name}(...$arguments);
        }
    }
}
