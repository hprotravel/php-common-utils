<?php

namespace Compass\Utils;

class JsonWrapper extends AbstractUtils
{
    /**
     * List of JSON Error messages assigned to constant names for better handling of version differences.
     */
    public static array $jsonErrorMessages = [
        'JSON_ERROR_DEPTH' => 'The maximum stack depth has been exceeded.',
        'JSON_ERROR_STATE_MISMATCH' => 'Invalid or malformed JSON.',
        'JSON_ERROR_CTRL_CHAR' => 'Control character error, possibly incorrectly encoded.',
        'JSON_ERROR_SYNTAX' => 'Syntax error.',
        'JSON_ERROR_UTF8' => 'Malformed UTF-8 characters, possibly incorrectly encoded.', // PHP 5.3.3
        'JSON_ERROR_RECURSION' => 'One or more recursive references in the value to be encoded.', // PHP 5.5.0
        'JSON_ERROR_INF_OR_NAN' => 'One or more NAN or INF values in the value to be encoded.', // PHP 5.5.0
        'JSON_ERROR_UNSUPPORTED_TYPE' => 'A value of a type that cannot be encoded was given.', // PHP 5.5.0
    ];

    /**
     * Encodes the given value into a JSON string.
     *
     * The method enhances `json_encode()` by supporting JavaScript expressions.
     * In particular, the method will not encode a JavaScript expression that is
     * represented in terms of a [[JsExpression]] object.
     *
     * Note that data encoded as JSON must be UTF-8 encoded according to the JSON specification.
     * You must ensure strings passed to this method have proper encoding before passing them.
     *
     * @param int $options the encoding options. For more details please refer to
     * <https://secure.php.net/manual/en/function.json-encode.php>. Default is `JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE`.
     */
    public static function encode(mixed $value, int $options = 0): string
    {
        $result = \json_encode($value, $options);

        JsonWrapper::handleJsonError(\json_last_error());

        return $result;
    }

    /**
     * Decodes the given JSON string into a PHP data structure.
     */
    public static function decode(mixed $json, bool $asArray = true): mixed
    {
        if (\is_array($json)) {
            throw new \InvalidArgumentException('Invalid JSON data.');
        } elseif ($json === null || $json === '') {
            return $asArray ? [] : null;
        }

        $decode = \json_decode((string)$json, $asArray);

        JsonWrapper::handleJsonError(\json_last_error());

        return $decode;
    }

    public static function pretty(mixed $json): string
    {
        return self::encode(self::decode($json), JSON_PRETTY_PRINT);
    }

    /**
     * Handles [[encode()]] and [[decode()]] errors by throwing exceptions with the respective error message.
     *
     * @param int $lastError error code from [json_last_error()](https://secure.php.net/manual/en/function.json-last-error.php).
     */
    protected static function handleJsonError(int $lastError): void
    {
        if (JSON_ERROR_NONE === $lastError) {
            return;
        }

        $availableErrors = [];
        foreach (JsonWrapper::$jsonErrorMessages as $const => $message) {
            if (\defined($const)) {
                $availableErrors[\constant($const)] = $message;
            }
        }

        if (isset($availableErrors[$lastError])) {
            throw new \InvalidArgumentException($availableErrors[$lastError], $lastError);
        }

        throw new \InvalidArgumentException('Unknown JSON encoding/decoding error.');
    }
}