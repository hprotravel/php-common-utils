<?php

namespace Compass\Utils;

final class HtmlUtils extends AbstractUtils
{
    public static function valid(string $string): bool
    {
        return preg_match("/<[^<]+>/", $string, $m) != 0;
    }

    public static function compress(string $buffer): string
    {
        if (\preg_match("/\<html/i", $buffer) == 1 && \preg_match("/\<\/html\>/i", $buffer) == 1) {
            $buffer = \preg_replace(['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s'], ['>', '<', '\\1'], $buffer);
        }

        return $buffer;
    }
}