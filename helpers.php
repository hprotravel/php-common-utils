<?php

use Compass\Utils\Collection;
use Compass\Utils\CommonUtils;
use Compass\Utils\CurrencyUtils;
use Compass\Utils\DateTimeUtils;
use Compass\Utils\ObjectUtils;
use Compass\Utils\Optional;
use Compass\Utils\StringUtils;

if (!function_exists('optional')) {
    /**
     * Allow arrow-syntax access of optional objects by using a higher-order
     * proxy object. That eliminates some need of ternary and null coalesce
     * operators.
     */
    function optional(mixed $value): Optional
    {
        return new Optional($value);
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     */
    function value(mixed $value, ...$args)
    {
        return $value instanceof Closure ? $value(...$args) : $value;
    }
}

if (!function_exists('collect')) {
    /**
     * Create a collection from the given value.
     */
    function collect(mixed $value = null): Collection
    {
        return new Collection($value);
    }
}

/** \Compass\Utils\CommonUtils helpers **/

if (!function_exists('retry')) {
    /**
     * Retry an operation a given number of times.
     *
     * @throws \Exception
     */
    function retry(
        array|int $times,
        callable $callback,
        int|\Closure $sleepMilliseconds = 0,
        ?callable $when = null,
    ) {
        return CommonUtils::retry($times, $callback, $sleepMilliseconds, $when);
    }
}

/** End \Compass\Utils\CommonUtils helpers **/

/** \Compass\Utils\ObjectUtils helpers **/

if (!function_exists('class_basename')) {
    /**
     * Getting class base (short) name from FQCN or object.
     */
    function class_basename(mixed $class): string
    {
        return ObjectUtils::classBasename($class);
    }
}

if (!function_exists('class_headline')) {
    function class_headline(mixed $class): string
    {
        return ObjectUtils::classHeadline($class);
    }
}

/** End \Compass\Utils\ObjectUtils helpers **/

/** \Compass\Utils\StringUtils helpers **/

if (!function_exists('str_interpolate')) {
    /**
     * Interpolate replacement values into the message.
     */
    function str_interpolate(string $message, array $context): string
    {
        return StringUtils::interpolate($message, $context);
    }
}

if (!function_exists('str_random')) {
    /**
     * Generate a more truly "random" alpha-numeric string.
     */
    function str_random(int $length = 8): string
    {
        return StringUtils::random($length);
    }
}

if (!function_exists('str_escape')) {
    /**
     * Escape single and double quotes and substitute.
     */
    function str_escape(string $string, $charset = 'UTF-8'): string
    {
        return StringUtils::escape($string, $charset);
    }
}

if (!function_exists('headline')) {
    /**
     * Convert the given string to title case for each word.
     */
    function headline(string $string): string
    {
        return StringUtils::headline($string);
    }
}

/** End \Compass\Utils\StringUtils helpers **/

/** \Compass\Utils\DateTimeUtils helpers **/

if (!function_exists('date_sub_interval')) {
    /**
     * Add date interval any given datetime object or create new one from string ad return formatted date string.
     */
    function date_sub_interval(
        string|DateTimeInterface $date,
        string $interval = 'P1M',
        string $format = 'Y-m-d',
    ): string {
        return DateTimeUtils::subtractInterval($date, $interval, $format);
    }
}

/** End \Compass\Utils\DateTimeUtils helpers **/

/** \Compass\Utils\CurrencyUtils helpers **/

if (!function_exists('currency_format')) {
    function currency_format(float|int $amount, string $code, string $locale = 'en'): string
    {
        return CurrencyUtils::format($amount, $code, $locale);
    }
}

/** End \Compass\Utils\CurrencyUtils helpers **/
